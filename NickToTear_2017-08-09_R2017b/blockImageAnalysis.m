%%  function imageAnalysis:
%  find the row projection of the left block ofimage and then based on the analysis
% returns img - smotthed image. index detemines which block of the image
% analysis will be performed first or 2nd block
%  Type 1 defect: determine if uneven edge of the strip can be detected.
%  Type 2 defect: knife crack can be detected on the image
% function [defType, img] = imageAnalysis(img)
% function [defType, img, S1ave] = imageAnalysis(img)
function img = blockImageAnalysis(img, imgNum, index)
if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end

%%imgTemp.bAligned = false;
%%[imgTemp.alignedCamImg, imgTemp.bAligned] = alignStripHorz(imgTemp.currImg);
% imgTemp.currImg = img_out;
% imgTemp.imgType = 0;

IblurY2 = imgaussfilt(imgTemp.currImg, [1 8]);
imgTemp.SmoothedImage = IblurY2;
% figure, imshow(imgTemp.SmoothedImage),title('SmoothedImage');
% imgTemp.currImg = SmoothedImage changes the original image input to this function
% imgTemp.SmoothedImage = imgTemp.currImg; % this assignment made for trial code to smooth an image.

%% find image block( 2 blocks start from 1 to 20 or 590 to 610 row projection
if(index == 1)
    Save1 = mean(imgTemp.currImg(:,1:20), 2); % row projection
    Save = Save1;
    SaveDeriv1 = diff(Save1);
    SaveDeriv = SaveDeriv1;
else
    Save2 = mean(imgTemp.currImg(:,590:610), 2); % row projection
    Save = Save2;
    SaveDeriv2 = diff(Save2);
    SaveDeriv = SaveDeriv2;
end % if(index
% S1mean = mean(imgTemp.Save);
%imgTemp.SaveDeriv = diff(imgTemp.Save);

%% find image row projection
%imgTemp.Save = mean(imgTemp.SmoothedImage, 2); % row projection
% S1mean = mean(imgTemp.Save);
SaveDeriv = diff(Save);
[N, edges] = histcounts(SaveDeriv, 3);
% figure, plot(imgTemp.Save);
% hold on
% plot(imgTemp.SaveDeriv);
% hold off;

%% now you use "getframe" and "frame2im"
%
% f = getframe(gca);
% im = frame2im(f);
%
%
% % rename this with prefix h1 or h2 in the current image filename
% filename = '1.png'; %default name
% if(imgNum == 1)
%     filename = strcat('Plot-Save-SDrv1_' , img.currFilename);
% else
%     filename = strcat('Plot-Save-SDrv1_' , img.currFilename);
% end
%
% imwrite(im,filename);

% % if the strip is at the corner of the image, donot consider the strip for
% % furthur analysis, as it might not have enough information
% [rS1ave, ~] = size(S1ave);
% if ((S1ave(1,1) >= 15) || (S1ave(rS1ave,1) >= 15))
%     %no defects can be determined in this case
%     imgTemp.imgType = 0;
%     return;
% end

% S2aveDeriv = diff(imgTemp.SaveDeriv);
% plot(S2aveDeriv);
if max(Save >= DefectConstants.MAX_AVG_PIXEL_VAL)
    %defects type
    imgTemp.imgType1 = 1;
else
    % strip is not in focus. furthur analysis cannot be done on this image
    %fprintf(img.fileID, '\n imageAnalysis::strip is not in focus. furthur analysis cannot be done on this image');
    disp('strip is not in focus. furthur analysis cannot be done on this image');
    imgTemp.imgType1 = 0;
    if(imgNum == 1)
        img.Cam1Img = imgTemp;
    else
        img.Cam2Img = imgTemp;
    end
    return;
end

% if(imgTemp.imgType == 1)
%     alignStripHorz
% end
%% define edge value
[maxCounts1, I10] = max(SaveDeriv);
[minCounts1, I11] = min(SaveDeriv);

% dupDerv = imgTemp.SaveDeriv;
%
%
% %define edge value
% [maxCounts2, I10] = find(imgTemp.SaveDeriv);
% [minCounts2, I11] = find(imgTemp.SaveDeriv);

%% find first edge
%if ((maxCounts1(1,1) - 5) > 0)
if ((maxCounts1(1,1) - 5) > 0)
    %TODO: find the number that goes to differ from maxCounts1 to find the edge
    %     [rEdge1, cEdge1] = find(imgTemp.SaveDeriv >= (maxCounts1(1,1) - 5) );
    [rEdge1, cEdge1] = find(SaveDeriv >= 5 );
else %if( maxCounts1(1,1) > 1)
    %[rEdge1, cEdge1] = find(imgTemp.SaveDeriv >= (maxCounts1(1,1) - 1) );
    %     cDefects = 0; %return cut-to-tear contrast error code
    % imgTemp.imgType = 0;
    if(imgNum == 1)
        img.Cam1Img = imgTemp;
    else
        img.Cam2Img = imgTemp;
    end
    % fprintf(img.fileID, '\n imageAnalysis::Strip Top edge not found');
    disp('Strip Top edge not found');
    return
end
if ((minCounts1(1,1) + 4) < 0)
    %TODO: find the number that goes to differ from minCounts1 to find the edge
    %     [rEdge2, cEdge2] = find(imgTemp.SaveDeriv <= (minCounts1(1,1) + 4));
    [rEdge2, cEdge2] = find(SaveDeriv <= (-4));
else
    %     cDefects = 0;   %return cut-to-tear contrast error code
    %fprintf(img.fileID, '\n imageAnalysis::Strip 2nd edge not found');
    disp('Strip 2nd edge not found');
    imgTemp.imgType1 = 0;
    return
end

if( isempty(rEdge1) || isempty(rEdge2) || isempty(cEdge1) || isempty(cEdge2) )
    %     cDefects = 0;       %edges not detected
    if(imgNum == 1)
        img.Cam1Img = imgTemp;
    else
        img.Cam2Img = imgTemp;
    end
    return
end
%% concatenate edges
RC = cat(1, rEdge1, rEdge2);
RC = sort(RC);

% % [corner1, c] = find(RC(:) <= DefectConstants.IND_IMG_ROW_TOP_THESH);
% % [corner2, c1] = find(RC(:) >= DefectConstants.IND_IMG_ROW_BTM_THESH);
% %
% % maxTopSave = max(Save(1:DefectConstants.IND_IMG_ROW_TOP_THESH));
% % maxBtmSave = max(Save(DefectConstants.IND_IMG_ROW_BTM_THESH:end));
% %
% % if( (~isempty(corner1)) || (~isempty(corner2)) || (maxTopSave >= DefectConstants.MAX_AVG_PIXEL_VAL) || (maxBtmSave >= DefectConstants.MAX_AVG_PIXEL_VAL) )
% %     % found edges but the edge is at border of the image
% %     % furthur analysis on this strip cannot be done as the
% %     % strip info is missing
% %     imgTemp.imgType1 = 0;
% %     %fprintf(img.fileID, '\n imageAnalysis::strip at corner of the image, with missing details. No algorithms applied. %d', imgTemp.imgType);
% %     disp('strip at corner of the image, with missing details. No algorithms applied.');
% %     disp(imgTemp.imgType1);
% %     if(imgNum == 1)
% %         img.Cam1Img = imgTemp;
% %     else
% %         img.Cam2Img = imgTemp;
% %     end
% %     %return;
% % end

%% scan for 3 different edges
[RCRows, ~] = size(RC);
EdgeCnt = 2;    %first value is the first edge we are looking at
for i = 2 : RCRows
    if( (RC(i, 1) - RC(i-1, 1)) > 10 )
        RC(EdgeCnt,1) = RC(i,1);
        EdgeCnt = EdgeCnt + 1;
    end
    if(EdgeCnt == 4)
        break;
    end
end

if(EdgeCnt == 4)
    %found 3 edges
    %fprintf(img.fileID,'\n imageAnalysis::EdgeIndices: \t %d \t %d \t %d', RC(1),RC(2),RC(3));
    disp('EdgeIndices');
    disp(RC(1));disp(RC(2));disp(RC(3));
    if(EdgeCnt >= RCRows)
        for i = EdgeCnt : RCRows
            RC(i, 1) = 0;
        end
    end
    imgTemp.imgType1= 2;
    if( (RC(1) <= 10) || (RC(2) <= 10) || (RC(3) <= 10) || (RC(1) >= 490) || (RC(2) >= 490) || (RC(3) >= 490) )
        % found 3 edges but the edge is at border of the image
        % furthur analysis on this strip cannot be done as the
        % strip info is missing
        %fprintf(img.fileID,'\n imageAnalysis::strip at corner of the image, with missing details. No algorithms applied.');
        disp('strip at corner of the image, with missing details. No algorithms applied.');
        imgTemp.imgType1 = 0;
    else
        
        if(index == 1)
            imgTemp.RC1 = RC(1:3, 1);
            imgTemp.RC1(:, 2) = 10;
            disp('RC1');
            disp(imgTemp.RC1);
        end
        if (index == 2)
            imgTemp.RC2 = RC(1:3, 1);
            imgTemp.RC2(:, 2) = 600;
            
            if (imgNum == 2)
                temp = imgTemp.RC1(:,1);
                imgTemp.RC1(:,1) = temp(:) + 503;
                temp = imgTemp.RC2(:,1);
                imgTemp.RC2(:,1) = temp(:) + 503;
            end% end of if
            
            disp('RC2');
            disp(imgTemp.RC2);
        end%end of if
        %     if (imgNum == 2)
        %         imgTemp.RC1(:)
        %     end% end of if
        
        
    end
    
else
    %didnot find 3 edges
    %     cDefects = 0;
    %fprintf(img.fileID,'\n imageAnalysis::Didnot find 3 edges of the strip. force the max value as 2nd edge now type  = 1');
    disp('Didnot find 3 edges of the strip. try another algorithm to find the third edge');
    % 2016-06-22 forcing  the values that we found so far as the 2nd border
    % of the image
    %     RC(3) = max(RC);
    %     RC(2) = mean([RC(1) RC(3)]);
    % %     RC(3) = RC(2);
    % %     if(index == 1)
    % %         imgTemp.RC1 = RC(1:3, 1);
    % %         imgTemp.RC1(:, 2) = 10;
    % %         disp('RC1');
    % %         disp(imgTemp.RC1);
    % %     end
    % %     if (index == 2)
    % %         imgTemp.RC2 = RC(1:3, 1);
    % %         imgTemp.RC2(:, 2) = 600;
    % %
    % %         if (imgNum == 2)
    % %             imgTemp.RC1(:,1) = imgTemp.RC1(:,1) + 503;
    % %             imgTemp.RC2(:,1) = imgTemp.RC2(:,1) + 503;
    % %         end% end of if
    % %
    % %         disp('RC2');
    % %         disp(imgTemp.RC2);
    % %     end%end of if
    % %
    % %     imgTemp.imgType1 = 1;
    % %     if(imgNum == 1)
    % %         img.Cam1Img = imgTemp;
    % %     else
    % %         img.Cam2Img = imgTemp;
    % %     end
    % %     return;
    
    
    if( EdgeCnt == 2 )
        %% we found only 2 edges, now find the third edge
        newSave = Save(RC(1,1):RC(i, 1));
        plot(newSave)
        hold on;
        sdiff = diff(newSave);
        plot(sdiff);
        hold off;
        %sortnew = sort(sdiff);
        [r,c] = min(sdiff);
        if(~isempty(c))
            % found 3rd edge thats the transition from nick to tear
            RC(2,1) = RC(1,1) + c; 
        else
            RC(2,1) = RC(RCRows,1);
        end
        RC(3,1) = RC(RCRows,1); 
        
        
        if(index == 1)
            imgTemp.RC1 = RC(1:3, 1);
            imgTemp.RC1(:, 2) = 10;
            disp('RC1');
            disp(imgTemp.RC1);
        end
        if (index == 2)
            imgTemp.RC2 = RC(1:3, 1);
            imgTemp.RC2(:, 2) = 600;
            
            if (imgNum == 2)
                imgTemp.RC1(:,1) = imgTemp.RC1(:,1) + 503;
                imgTemp.RC2(:,1) = imgTemp.RC2(:,1) + 503;
            end% end of if
            
            disp('RC2');
            disp(imgTemp.RC2);
        end%end of if
        
    end
end

%% finally at the end of this function save the changes made to individual
% image object
if(imgNum == 1)
    img.Cam1Img = imgTemp;
else
    img.Cam2Img = imgTemp;
end

end % end of function