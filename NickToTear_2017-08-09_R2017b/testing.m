I = imread('K_1149543680_00005.png');
imshow(I);

n=fix(size(I,1)/2);
I1 = I(1:n-1,:,:);
imhist(I1);

t = otsuthresh(histcounts(I1,-0.5:255.5));

hold on
plot(255*[t t], ylim, 'r', 'LineWidth', 5)
hold off

imshow(imbinarize(I,t))