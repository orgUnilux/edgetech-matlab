function n2tfindfiles()

% choose the folder for defect setings file
% Program Files (x86) or Program Files
%path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\temp';
%path_32b = 'C:\Program Files\Unilux\EdgeTech\config\temp';
% if(isdir(path_64b))
%     fullpath = path_64b;
% else
%     fullpath = path_32b;
% end

%% temp folder in images folder
%find temp folder
driveLetter = FindDriveInETConfig();
temppath = ':\Images\\temp';
if( ~isempty(driveLetter) )
    fullpath = strcat(driveLetter,temppath);    
end

while(1)

Files=dir('*.png'); %commented for findImagesFiles
[rcnt, ccnt] = size(Files);
if(rcnt)
    for indx = 1:rcnt
        filename = Files(indx).name;
        I = imread(filename);
        %img = imageInfo(I);
        N2TMainFunction(filename);
        disp(filename);
        % delete the file from the PC
        delete filename;
    end % end of for
end % end of if
%end of files that are read refresh and check if new files arrived
end
end% of function