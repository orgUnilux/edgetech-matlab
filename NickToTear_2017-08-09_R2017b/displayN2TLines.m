%%Display nick to tear on both the strips
% by reading from the csv file
% function displayN2TBoundary(cDefects, img, B)
function displayN2TLines(img)

eIndices = readN2TIndicesFromCSV();
colCnt = 10;
cDefects = eIndices;
[r c] = size(cDefects);
imshow(img.Img, [] );

hold on

for indx=1:1:3
    lineSpecsy= [img.Cam1Img.RC2(indx,1), img.Cam1Img.RC1(indx,1)];
    lineSpecsx = [img.Cam1Img.RC2(indx,2), img.Cam1Img.RC1(indx,2)];
    % plot(eIndices(:,2), eIndices(:,1),'y','LineWidth',2);
    % plot( img.Cam1Img.RC1(1), 10, img.Cam1Img.RC1(2), 600, 'y', 'LineWidth', 2);
    % plot( img.Cam1Img.RC2(1), 10, img.Cam1Img.RC2(2), 600, 'y', 'LineWidth', 2);
    line(lineSpecsx, lineSpecsy);
    % plot( (1:1296), img.n2tIndices(4,:), 'y', 'LineWidth', 2);
    % plot( (1:1296), img.n2tIndices(5,:), 'y', 'LineWidth', 2);
    % plot( (1:1296), img.n2tIndices(8,:), 'y', 'LineWidth', 2);
end%end of for

for indx=1:1:3
    lineSpecsy= [img.Cam2Img.RC2(indx,1), img.Cam2Img.RC1(indx,1)];
    lineSpecsx = [img.Cam2Img.RC2(indx,2), img.Cam2Img.RC1(indx,2)];
    % plot(eIndices(:,2), eIndices(:,1),'y','LineWidth',2);
    % plot( img.Cam1Img.RC1(1), 10, img.Cam1Img.RC1(2), 600, 'y', 'LineWidth', 2);
    % plot( img.Cam1Img.RC2(1), 10, img.Cam1Img.RC2(2), 600, 'y', 'LineWidth', 2);
    line(lineSpecsx, lineSpecsy);
    % plot( (1:1296), img.n2tIndices(4,:), 'y', 'LineWidth', 2);
    % plot( (1:1296), img.n2tIndices(5,:), 'y', 'LineWidth', 2);
    % plot( (1:1296), img.n2tIndices(8,:), 'y', 'LineWidth', 2);
end%end of for

x = img.n2TPoints(:,1);
y = img.n2TPoints(:,2);

plot(y,x,'c*');

hold off


end %end of function