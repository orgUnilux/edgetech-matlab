classdef imageInfo
    % imageinfo - class for image credentials
    
    % Properties
    properties
        currFilename  % current file name
        currFilepath  % current file path
        %         imgNumber     % in current file strip number 1 or 2
        Img             % image complete image wqith 2 strips read from drive
        iFullWidth        %image width in pixels
        iFullHeight       %image height in pixels
        
        below_thresh	% BELOW_THRESH value saved here. Incase external 
                        % defination changes this value
        burr_avg_threshold
        
        %% read text file inputs from csv file and save
        n2tIndices      % nick to tear indices of both the strips
        n2TPoints 
        readInFull
        txtCrackVals
        txtBurrVals
        txtDefCnt
        
        
        %% camra images details ( class object)
        Cam1Img       % cam1 image details
        Cam2Img         % cam2 image details
        
        %fileID        % write output to the file
    end % end of properties
    
    % Methods
    methods
        %function imgInfo = imageInfo(img, filename, filepath, fileID)
        %function imgInfo = imageInfo(img, filename, filepath)
        function imgInfo = imageInfo(img)
            imgInfo.Img = img;
%             imgInfo.currFilename = filename;
%             imgInfo.currFilepath = filepath;
            if ~isempty(imgInfo.Img)
                [r, c] = size(imgInfo.Img);
                imgInfo.iFullWidth = c;
                imgInfo.iFullHeight = r;
            else
                %no input image
                return;
            end
            imgInfo.Cam1Img = indImgInfo(1);
            imgInfo.Cam2Img = indImgInfo(2);
            
            imgInfo.below_thresh = DefectConstants.BELOW_THRESH;
            imgInfo.burr_avg_threshold = DefectConstants.BURR_AVG_THRESHOLD;
            
            n=fix(size(imgInfo.Img,1)/2);
            imgInfo.Cam1Img.currImg =  imgInfo.Img(1:n-1,:,:);
            imgInfo.Cam2Img.currImg =  imgInfo.Img(n+3:end,:,:);
            
            imgInfo.Cam1Img = Set_Defaults(imgInfo.Cam1Img);
            imgInfo.Cam2Img = Set_Defaults(imgInfo.Cam2Img);
            %imgInfo.fileID  = fileID; % save text output file id
%             %create and open a file
%             mkdir('Defects'); % create defects directory
%             imgInfo.fileID = fopen('Defects\\exp.txt','w');
            imgInfo.n2tIndices = zeros(8, imgInfo.iFullWidth);
        end % end of imageInfo constructor
    end % end of methods
end % end of class



