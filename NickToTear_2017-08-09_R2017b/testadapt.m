function testadapt()

 I = imread('S_1160079820_00048.png');
 n=fix(size(I,1)/2);
 A1=I(1:n-1,:,:);
 A2=I(n+3:end,:,:);
 
 [BWTS, cutBin] = adaptBlocks(A1);

end