function [BWTS, cutBin] = adaptBlocks(I)
%I = imread('401.png');
% figure, imshow(I);

%SmoothedImage = img;
SmoothedImage = imgaussfilt(I, [1 2]);
% figure, imshow(SmoothedImage),title('SmoothedImage');

seg_out  = blkproc(SmoothedImage, [500,DefectConstants.ADAPT_BLK_WIDTH],@adaptt);
% seg_out  = blkproc(SmoothedImage, [500,2],@adaptt);

seg = im2bw(seg_out);
properties = regionprops(seg, {'Area', 'Eccentricity', 'EquivDiameter', 'EulerNumber', 'MajorAxisLength', 'MinorAxisLength', 'Orientation', 'Perimeter'});

%cutBin = zeros(seg_out(:));
% I(indarray{index}) = 0;
% imshow(I_out);

% Getting the number of pixels in each threshold, assuming the background
% has the most pixels
binaryLay1 = (seg_out ==1);
binaryLay2 = (seg_out ==2);
binaryLay3 = (seg_out ==3);

% imtool(binaryLay1);
% imtool(binaryLay2);
% imtool(binaryLay3);

indarray = {binaryLay1(:), binaryLay2(:), binaryLay3(:)};

[m ,index] = max([sum(binaryLay1(:)) sum(binaryLay2(:)) sum(binaryLay3(:))]); 
[m ,cutIndx] = min([sum(binaryLay1(:)) sum(binaryLay2(:)) sum(binaryLay3(:))]); 
%Remove the background
seg_out(indarray{index}) = 0;
seg(indarray{index}) = 0;
cutBin = seg;   % initialise
% tearBin = seg;  % initialise

% get only cut regoin in cutBin
for i = 1 : 3
    if ( i ~= cutIndx)
        cutBin(indarray{i}) = 0; % assuming cut regoin is small on object
    end
end

% tearBin = seg - cutBin;

% imtool(cutBin);
I(indarray{index}) = 0;

% Convert to binary
BWTS = seg_out>0;
% figure;
% imshow(BWTS)
% axis off

% this is of type double convert into binary for applying Image Region
% analyzer
% BW_out = im2bw(I_out);
% figure, imshow(BWTS), title('adaptMethod');
end