%% this function reads the tet file input
% text file has sensitivity parameters for
% image defect detection - crakc and burr

%%function start
%function applyTextFileInput()

function img = applyTextFileInput(img)

img.below_thresh = img.txtCrackVals(2);
img.burr_avg_threshold = img.txtBurrVals(2);

% % 
% % %code testing
% % filecnt = 4;
% % img.readInFull(4,3) = filecnt;
% % 
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', img.readInFull);
% % %csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', img.txtDefCnt, 3, 2 );


end % end of function