%%Display nick to tear on both the strips
% by reading from the csv file
% function displayN2TBoundary(cDefects, img, B)
function displayN2TBoundary(img)

eIndices = readN2TIndicesFromCSV();
colCnt = 10;
cDefects = eIndices;
[r c] = size(cDefects);
imshow(img.Img, [] );

hold on
% plot(eIndices(:,2), eIndices(:,1),'y','LineWidth',2);
plot( (1:1296), img.n2tIndices(1,:), 'y', 'LineWidth', 2);
plot( (1:1296), img.n2tIndices(4,:), 'y', 'LineWidth', 2);
plot( (1:1296), img.n2tIndices(5,:), 'y', 'LineWidth', 2);
plot( (1:1296), img.n2tIndices(8,:), 'y', 'LineWidth', 2);
hold off

% for indx = 1 :2: colCnt
%     % Draw boundary line on defect edge
%     %     plot(B(cDefects(1, 1): cDefects(1, 2),2),B(cDefects(1, 1): cDefects(1, 2),1),'g','LineWidth',2);
%     if((indx+1) <= colCnt)
%         if (cDefects(1, indx) > 0) && (cDefects(1, indx+1) > 0)
%             cstartIndx = cDefects(1, indx);
%             %fprintf(img.fileID, '\n displayFinalDefects::Draw boundary line on defect cstartIndxedge %d ', cstartIndx);
%             disp(cstartIndx);
%             [startIndx,~] = find(eIndices(:, 2) == cstartIndx);
%             
%             %                     % if this is 2nd image find the indexes w.r.t. complete
%             %                     % image
%             %                     if(imgNum == 2)
%             %
%             %                     end % end of if
%             if indx < colCnt
%                 cendIndx = cDefects(1, indx+1);
%                 [endIndx, ~] = find(eIndices(:, 2) == cendIndx);
%             else
%                 break;
%             end
%         end
%     end
%     %         if( (startIndx ~= 0 ) && (endIndx ~= 0))
%     if( ~isempty(startIndx) && ~isempty(endIndx))
%         if( (startIndx(1,1) ~= 0 ) && (endIndx(1,1) ~= 0))
%             plot(eIndices(startIndx(1,1):endIndx(1,1),2), eIndices(startIndx(1,1):endIndx(1,1),1),'y','LineWidth',2);
%             
%             %[low, high] = findLowHigh(eIndices, startIndx(1,1))
%             % highlight(eIndices(startIndx(1,1):endIndx(1,1),2), eIndices(startIndx(1,1):endIndx(1,1),1)) = 255;
%                         
%             %reinitialize
%             startIndx = 0;
%             endIndx = 0;
%         end
%     end
%     % Draw bounding box around region
%     %rectangle('Position',200, 200, 50 , 50,'EdgeColor','w')
%     %        rectangle('Position',[830, 240, 200 , 150],...
%     % 	'Curvature',[0,0],...
%     % 	'EdgeColor', 'r',...
%     % 	'LineWidth', 3,...
%     % 	'LineStyle','-')
%     
% end % endof for

end %end of function