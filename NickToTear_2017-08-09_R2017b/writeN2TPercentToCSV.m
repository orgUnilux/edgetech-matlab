%% writes nick to tear percent in csv file, 
% EdgeTech will read on other end to display as
% percentage values on UI display boxes
%
%%
function img = writeN2TPercentToCSV(img)

% choose the folder for defect setings file
% Program Files (x86) or Program Files
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'NickToTear.csv';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);
else
    fullpath = strcat(path_32b, settingFilename);
end

readInFull = [img.Cam1Img.RC1; img.Cam1Img.RC2; img.Cam2Img.RC1; img.Cam2Img.RC2];

img.n2TPercents= readInFull;
csvwrite(fullpath, readInFull); % write settings to the file
% % %
% % readInFull(4,3) = filecnt;
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('DefectSettings.csv', readInFull);
close all;

end %end of function
