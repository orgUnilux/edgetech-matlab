%function findImageFiles( Input )
function findImageFiles( Files )
%findImageFiels will find .png files from current folder and sub folders
% Filenames = zeros(10000);
%newInput = Input{:};
%Files=dir(newInput);
Files=dir('*');
%indx = 1;
% Filenames = Files(:).name;
% x = char(zeros(2,100));

% if( ~isdir('F:\Defects') )
%     mkdir('F:\Defects'); % create defects directory
% end
% if( ~isdir('F:\GoodSamples') )
%     mkdir('F:\GoodSamples'); % create GoodSamples directory
% end

% if( ~isdir('Defects') )
%     mkdir('Defects'); % create defects directory
% end
% if( ~isdir('GoodSamples') )
%     mkdir('GoodSamples'); % create GoodSamples directory
% end
% fileID = 0;
pathid = 0;
dFlag = false;
filecnt = 0;
for k=1:length(Files)
    CurrName = Files(k).name;
    if( isdir(CurrName) && ~strcmp(CurrName,'.') && ~strcmp(CurrName,'..') )
        %% if this is directory
        %%read sub files
        subFolderPath = strcat(CurrName, '\\');
        
% %         %create and open a file
% %         
% %         txtfilename = strcat('Defects\\', CurrName , '1.txt');
% %         if(fileID)
% %             fclose(fileID);
% %             fileID = 0;
% %         end
% %         fileID = fopen(txtfilename,'w');
        
        subFiles=dir(subFolderPath);
        
        for subk=1:length(subFiles)
            
            currSubFile = subFiles(subk).name;
            if(strfind(currSubFile,'.png'))
                %                 Filenames(indx) = strcat(subFolderPath, CurrFileName);
                %                 indx = indx + 1;
                CurrFilePath = strcat(subFolderPath, currSubFile);
                disp(CurrFilePath);
                dFlag = false;
                %dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );            
                dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath );                                
                if(dFlag)
                    filecnt = filecnt  +1;
                end
                disp(' defect files count : ');
                disp(filecnt);
                                                                                         
            else
                
                CurrFilePath = strcat(subFolderPath, currSubFile);
                
                if( isdir(CurrFilePath) && ~strcmp(currSubFile,'.') && ~strcmp(currSubFile,'..') )
                    %% if this is directory
                    %%read sub files
                    ssubFolderPath = strcat(CurrFilePath, '\\');
                    
%                     %create and open a file
%                     
%                     txtfilename = strcat('Defects\\', currSubFile , '1.txt');
%                     if(fileID)
%                         fclose(fileID);
%                         fileID = 0;
%                     end
%                     fileID = fopen(txtfilename,'w');
                    
                    ssubFiles=dir(ssubFolderPath);
                    
                    for ssubk=1:length(ssubFiles)
                        
                        currsSubFile = ssubFiles(ssubk).name;
                        if(strfind(currsSubFile,'.png'))
                            %                 Filenames(indx) = strcat(subFolderPath, CurrFilePath);
                            %                 indx = indx + 1;
                            CurrsFileName = strcat(ssubFolderPath, currsSubFile);
                            disp(CurrsFileName);
                            dFlag = false;
                            %dFlag = KCrackMainFunction( ssubFolderPath, currsSubFile, CurrsFileName,fileID );                       
                            dFlag = KCrackMainFunction( ssubFolderPath, currsSubFile, CurrsFileName );                       
                                                         
                            if(dFlag)
                                filecnt = filecnt  +1;
                            end
                            disp(' defect files count : ');
                            disp(filecnt);        
                                                                                                             
                        end   %end of if
                    end %end of for
                end % end of if              
                                              
            end % end of if
        end %end of for
    else
        if(strfind(CurrName,'.png'))
            %% this is filename copy file name
            %             Filenames(indx) = CurrName;
            %             indx = indx + 1;
            CurrFilePath = CurrName;
            disp(CurrFilePath);
            dFlag = KCrackMainFunction( currsSubFile, CurrFilePath, CurrsFileName );                        
                                                         
            if(dFlag)
                filecnt = filecnt  +1;
            end
            disp(' defect files count : ');
            disp(filecnt);
        end
    end
    %if(fileID)
    %fclose(fileID );
    %fileID = 0;
    %end
end % end of for

% choose the folder for defect setings file
% Program Files (x86) or Program Files 
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'DefectSettings.csv';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);    
else
    fullpath = strcat(path_32b, settingFilename);   
end
readInFull = csvread(fullpath);
readInFull(4,3) = filecnt;
csvwrite(fullpath, readInFull); % write settings to the file
% % %
% % readInFull(4,3) = filecnt;
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('DefectSettings.csv', readInFull);
close all;
end % end of function