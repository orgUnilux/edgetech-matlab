%% this function reads the csv file 
% csv file has nic to tear values
% 

%%function start
function indices = readN2TIndicesFromCSV()

% choose the folder for nickToTear file
% Program Files (x86) or Program Files 
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'NickToTear.csv';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);    
else
    fullpath = strcat(path_32b, settingFilename);   
end

indices = csvread(fullpath);

end % end of function