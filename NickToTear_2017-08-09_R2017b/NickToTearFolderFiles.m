%% function NickToTearFolderFiles()
% this function reads from the 'NickToTearConfig.txt' file
% the path of the image file. then processes the information and
% writes the nick to tear in % and indices in NickToTearSettings.csv
% file.

function NickToTearFolderFiles(  )

% choose the folder for nick to tear setings file
% Program Files (x86) or Program Files 
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'NickToTearConfig.txt';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);    
else
    fullpath = strcat(path_32b, settingFilename);   
end

disp('from file at ');
disp(fullpath);
fileid = fopen(fullpath, 'r');
path = fscanf(fileid, '%s');   % read path written by edgetech 
fclose(fileid);

disp('path read from file ' );
disp(path);

Files=dir(path);
n2tPath = strcat(path,'\\');

dFlag = false;
filecnt = 0;
for k=1:length(Files)
    CurrName = Files(k).name;
    if(strfind(CurrName,'.png'))
       
        CurrFilePath = strcat(n2tPath, CurrName);
        
        dFlag = false;
        %dFlag = N2TMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );
        dFlag = N2TMainFunction(n2tPath, CurrName, CurrFilePath );
        if(dFlag)
            filecnt = filecnt  +1;
        end
        disp(' defect files count : ');
        disp(filecnt);             
    end       
end % for k=...



end %end of function