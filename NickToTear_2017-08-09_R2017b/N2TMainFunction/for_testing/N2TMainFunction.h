//
// MATLAB Compiler: 6.1 (R2015b)
// Date: Wed Apr 26 15:30:18 2017
// Arguments: "-B" "macro_default" "-W" "cpplib:N2TMainFunction" "-T"
// "link:lib" "-d"
// "G:\NickToTear_2017-04-21_R2015b\N2TMainFunction\for_testing" "-v"
// "G:\NickToTear_2017-04-21_R2015b\N2TMainFunction.m" 
//

#ifndef __N2TMainFunction_h
#define __N2TMainFunction_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#include "mclcppclass.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_N2TMainFunction
#define PUBLIC_N2TMainFunction_C_API __global
#else
#define PUBLIC_N2TMainFunction_C_API /* No import statement needed. */
#endif

#define LIB_N2TMainFunction_C_API PUBLIC_N2TMainFunction_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_N2TMainFunction
#define PUBLIC_N2TMainFunction_C_API __declspec(dllexport)
#else
#define PUBLIC_N2TMainFunction_C_API __declspec(dllimport)
#endif

#define LIB_N2TMainFunction_C_API PUBLIC_N2TMainFunction_C_API


#else

#define LIB_N2TMainFunction_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_N2TMainFunction_C_API 
#define LIB_N2TMainFunction_C_API /* No special import/export declaration */
#endif

extern LIB_N2TMainFunction_C_API 
bool MW_CALL_CONV N2TMainFunctionInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_N2TMainFunction_C_API 
bool MW_CALL_CONV N2TMainFunctionInitialize(void);

extern LIB_N2TMainFunction_C_API 
void MW_CALL_CONV N2TMainFunctionTerminate(void);



extern LIB_N2TMainFunction_C_API 
void MW_CALL_CONV N2TMainFunctionPrintStackTrace(void);

extern LIB_N2TMainFunction_C_API 
bool MW_CALL_CONV mlxN2TMainFunction(int nlhs, mxArray *plhs[], int nrhs, mxArray 
                                     *prhs[]);


#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

/* On Windows, use __declspec to control the exported API */
#if defined(_MSC_VER) || defined(__BORLANDC__)

#ifdef EXPORTING_N2TMainFunction
#define PUBLIC_N2TMainFunction_CPP_API __declspec(dllexport)
#else
#define PUBLIC_N2TMainFunction_CPP_API __declspec(dllimport)
#endif

#define LIB_N2TMainFunction_CPP_API PUBLIC_N2TMainFunction_CPP_API

#else

#if !defined(LIB_N2TMainFunction_CPP_API)
#if defined(LIB_N2TMainFunction_C_API)
#define LIB_N2TMainFunction_CPP_API LIB_N2TMainFunction_C_API
#else
#define LIB_N2TMainFunction_CPP_API /* empty! */ 
#endif
#endif

#endif

extern LIB_N2TMainFunction_CPP_API void MW_CALL_CONV N2TMainFunction(int nargout, mwArray& dFlag, const mwArray& I);

#endif
#endif
