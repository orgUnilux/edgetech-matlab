%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FindDriveInETConfig function finds the iamges folder drive
% from EdgeTech SiteFile.cfg and return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function driveLetter = FindDriveInETConfig()
siteFileName = 'SiteFile.cfg';
config_86 = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
siteFile_86 = strcat(config_86, siteFileName);
config_64 = 'C:\Program Files\Unilux\EdgeTech\config\';
siteFile_64 = strcat(config_64, siteFileName);

siteFullpath = siteFile_86;
%cfg  = fopen('C:\Program Files (x86)\Unilux\EdgeTech\config\SiteFile.cfg');
cfg = fopen(siteFullpath);
if(cfg <= -1)
    %was not successful in opening config file, trying in another location
    siteFullpath = siteFile_64;
    cfg = fopen(siteFullpath);
    %cfg  = fopen('C:\Program Files\Unilux\EdgeTech\config\SiteFile.cfg');
end
%line = fgetl(cfg); %this line reads the first line, you can discard it right away
% find the correct directory and file
%io_contents = ...
% fullfile('C:\Program Files\Unilux\EdgeTech\','config','SiteFile.cfg');
io_contents = siteFullpath;
% read the file
filetext = fileread(io_contents);

% search for the line  that includes ':'
% each line is separated by a newline ('\n')

expr = '[^\n]*:[^\n]*';
fileread_info = regexp(filetext,expr,'match');

r1c1 = fileread_info{1,1};
ss = char(r1c1);
LetterIndex = find(ss == ':');
if( (r1c1(LetterIndex) >= 'A') || (r1c1(LetterIndex) <= 'Z') )
    % if we found the drive letter
    driveLetter = r1c1(LetterIndex - 1);
else
    % if we didnot find the drive letter
    driveLetter = 'D'; %default letter assigned
end
end % end of function