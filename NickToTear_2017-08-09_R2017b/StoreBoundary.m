%% this function stores the nick to tear indices of the image in a single 
% variable, in order to save it to the csv file

function img = StoreBoundary(img, imgNum)

if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end

%%
% if ~( (max(imgTemp.eIndices(:,2)) == imgTemp.iWidth) && (min(imgTemp.eIndices(:,2)) == 1) )
%     imgTemp.eIndices = 0;
%     imgTemp.eIndices = findInd(BW_out);
% end

% find nick region boundary indices
eN2TBoundary = findInd(imgTemp.IBinNick);

% combine the indices
imgTemp.eN2TBoundary = [imgTemp.eIndices; eN2TBoundary]; 

% finally at the end of this function save the changes made to indivudal
% image object
if(imgNum == 1)
    img.Cam1Img = imgTemp;
else
    img.Cam2Img = imgTemp;
end

end % store the boundary