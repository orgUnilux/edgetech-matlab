function img = writeN2TIndicesToCSV(img, ImgNum)

% choose the folder for defect setings file
% Program Files (x86) or Program Files
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'NickToTear.csv';

stripnickTopIndex = 0;
stripnickBottomIndex = 0;
striptearTopIndex = 0;% some times this cound be different
striptearBottomIndex = 0;

%% define a matrix to save indices
% format: first two rows consists of nick region indices of top strip
% 2nd and 3rd row consists of tear region indices of top strip
%  4th and 5th two rows consists of nick region indices of bottom strip
% 5th and 6th row consists of tear region indices of bottom strip
%%
% img.n2tIndices = zeros(8, img.iFullWidth);
if(ImgNum == 1)
    stripnickTopIndex = 1;
    stripnickBottomIndex = 2;
    striptearTopIndex = 3;% some times this cound be different
    striptearBottomIndex = 4;
else
    stripnickTopIndex = 5;
    stripnickBottomIndex = 6;
    striptearTopIndex = 7;% some times this cound be different
    striptearBottomIndex = 8;
end

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);
else
    fullpath = strcat(path_32b, settingFilename);
end

%% find min max values of each indx
%readInFull = csvread(fullpath);
%readInFull(4,3) = filecnt;

if(ImgNum ==1)
    % if first image just write this image nick to tear indices
    readInFull = img.Cam1Img.eIndices;
    [rInd, ~] = size(readInFull);
    % %     for indx =1:1:rInd
    % %        [~, ~, Val] =  find(readInFull(:,2) == indx);
    % %        maxVal = max(Val);
    % %        minVal = min(Val);
    % %        img.n2tIndices(stripnickTopIndex, indx) = minVal;
    % %        img.n2tIndices(stripnickBottomIndex, indx) = maxVal;
    % %     end % end of for
else
        readInFull = img.Cam2Img.eIndices;
    [rInd, ~] = size(readInFull);
% %     % if its 2nd image write the two strip indices
% %     secondStripEindices = img.Cam2Img.eIndices;
% %     secondStripEindices(:,1) = img.Cam2Img.eIndices(:,1) + 503;
% %     
% %     readInFull = [img.Cam1Img.eIndices;  secondStripEindices];
    
    % %         readIndices = img.Cam2Img.eIndices;
    % %        img.n2tIndices(stripnickTopIndex, indx) = minVal;
    % %        img.n2tIndices(striptearBottomIndex, indx) = maxVal;
end% end of if

for indx = 1 : 1 : img.iFullWidth
    [rIndx, ~] =  find(readInFull(:,2) == indx);
    currVals = readInFull(rIndx(:), 1); % our aim is to find the first column of readInFull
    maxVal = max(currVals);
    minVal = min(currVals);
    img.n2tIndices(stripnickTopIndex, indx) = minVal;
    img.n2tIndices(striptearBottomIndex, indx) = maxVal;
end % end of for

% %% if 2nd strip add the hard coded 503 value tot he indices(to indicate 2nd
% % strip on bottom)
if(ImgNum == 2)
    img.n2tIndices(stripnickTopIndex, :) = img.n2tIndices(stripnickTopIndex, :) + 503;
    img.n2tIndices(striptearBottomIndex, :) = img.n2tIndices(striptearBottomIndex, :) + 503;
end % end of if
csvwrite(fullpath, readInFull); % write settings to the file
% % %
% % readInFull(4,3) = filecnt;
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('DefectSettings.csv', readInFull);
close all;

end %end of function
