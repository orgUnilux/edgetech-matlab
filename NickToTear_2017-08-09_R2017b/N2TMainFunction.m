%% read images in
%  script has few hard coded values based on the current EdgeTech camera
% image standard size i.e. 1003 X 1296
% function dFlag = N2TMainFunction( subFolderPath, CurrFileName, CurrFilePath, fileID )
function N2TMainFunction( FileName )
%function dFlag = N2TMainFunction(  )
% Filenames = zeros(10000, 2);
%  Files=dir('*.png'); %commented for findImagesFiles
%  [rcnt, ccnt] = size(Files);
%  filename = Files(1).name;
%dFlag = false;

%% read specific file
I = imread(FileName);
%I = imread('Unilux-20160502_1450.png');
%I = imread('Unilux-20170515_1042.png');
% I = imread('S_1160079820_00048.png');
[~, ~, ch] = size(I);
if ch == 3
    %find if this is rgb or gray image
    % if rgb convert to grayscale
    newI = rgb2gray(I);
    %I = 0;
    I = newI;
end %end of if ch==3

%%remove noise from the first row
I(1, :) = I(2, :);
I(504, :) = I(505, :);
%img = imageInfo(I, CurrFileName, CurrFilePath, fileID);
%img = imageInfo(I, CurrFileName, CurrFilePath);
img = imageInfo(I);

%% read and apply from the csv text file input
%readN2TIndicesFromCSV(img);
%img = applyTextFileInput(img);

close all;  % close all windows
%         figure,imshow(A1), title(FileName);
%   determine if type 1 and 2 defects are eligible on this image
img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting
%     histBlockMethod(A1);
%         [BWEdge, A11_uni, eIndices, nickBin] = AdaptMethod(A1);   % extract strip and find uneven edge
if((img.Cam1Img.imgType == 1) || (img.Cam1Img.imgType == 2))
    img = AdaptMethod(img, img.Cam1Img.imgNumber);   % extract strip and find uneven edge
    img = findStartEndPoints(img, img.Cam1Img.imgNumber);
%     img = writeN2TIndicesToCSV(img, img.Cam1Img.imgNumber);
%     img = writeN2TPointsToCSV(img, img.Cam1Img.imgNumber);
    %         bTwoLayers = false;
    %         [type, A1, S1Ave] = imageAnalysis(A1);%changed function returns for plotting
    img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting
end

close all;  % close all windows


%
%determine if type 1 and 2 defects are eligible on this image

img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
if((img.Cam2Img.imgType == 1) || (img.Cam2Img.imgType == 2))
    
    img = AdaptMethod(img, img.Cam2Img.imgNumber);     % extract strip and find uneven edge
    img = findStartEndPoints(img, img.Cam2Img.imgNumber);
    %     img = writeN2TIndicesToCSV(img, img.Cam2Img.imgNumber);
%     img = writeN2TPointsToCSV(img, img.Cam2Img.imgNumber);
    %
    %determine if type 1 and 2 defects are eligible on this image
    %
    img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
end

% choose the folder for defect setings file
% Program Files (x86) or Program Files
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'NickToTear.csv';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);
else
    fullpath = strcat(path_32b, settingFilename);
end
%StoreBoundary(img, img.Cam2Img.imgNumber);
%readInFull(4,3) = filecnt;
%csvwrite(fullpath, readInFull); % write settings to the file
img = writeN2tPercentToCSV(img);
%img = writeN2tValueToCSV(img);
%%img = writeN2TPointsToCSV(img); % commented not available for edgetech
%%displayN2TBoundary(img)
%displayN2TLines(img);

end %end of function
% disp('End of KnifeCrackMainFunction');
%%
% disp(filecnt);
