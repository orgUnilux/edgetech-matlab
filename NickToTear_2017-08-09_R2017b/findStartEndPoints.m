
%% function findStartEndPoints determines the start and endpoints of the
% nick to tear boundaries.
% these indices are used to draw a line in EdgeTech fro user to see the
% nick and tear
%%
function img = findStartEndPoints(img, imgNum)
if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end

img = blockImageAnalysis(img, imgNum, 1);

img = blockImageAnalysis(img, imgNum, 2);


end % end of function