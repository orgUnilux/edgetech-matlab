function [BW2, eIndices] = extractLayersMethod()

A = imread('S_1160079820_00048.png');

% Smoothing
A = imgaussfilt(A, [1 8]);

thresh = multithresh(A, 2);
seg = imquantize(A,thresh);

% Getting the number of pixels in each threshold, assuming the background
% has the most pixels
ind1 = (seg ==1);
ind2 = (seg ==2);
ind3 = (seg ==3);

imtool(ind1);
imtool(ind2);
imtool(ind3);

indarray = {ind1(:), ind2(:), ind3(:)};

[m ,index] = max([sum(ind1(:)) sum(ind2(:)) sum(ind3(:))]); 

%Remove the background
seg(indarray{index}) = 0;

% Convert to binary
BWTS = seg>0;
figure;
imshow(BWTS)
axis off
[BWEdge, maskedEdgeImage1] = fillHoles(BWTS);

% Getting rid of noise using morphological operations
se =  strel('disk',3);
BWTS1 = imopen(BWTS, se);
    
%% Remove any noise
bw = bwareaopen(BWTS1,10000);
BW_out = imfill(bw, 'holes');
figure, imshow(BW_out), title('AdaptMethod- Backgrd');
BW2 = bwperim(bw);
%% find a good starting point for tracing the boundary
[row,col] = find(BW2 >0,1);
eIndices = bwtraceboundary(BW2,[row,col],'E',8);
imshow(BW2);
hold on;
plot(eIndices(:,2),eIndices(:,1),'g','LineWidth',2);
end
