%% find uneven edge
% function [cDefects] = UnevenEdge( BWEdge, eIndices )
% function [newBMin] = UnevenEdge( BWEdge, eIndices )
function img = UnevenEdge(img, imgNum)
if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end
% [imHeight, imWidth] = size(BWEdge);
[BHeight, ~] = size(imgTemp.eIndices );
imgTemp.newBMin = zeros(size(imgTemp.IBin(1,:)));
%B matrix has first column as pixel value
% 2nd column as the y coordinate
% extract values till image width (here 1296)
for i = 1 : imgTemp.iWidth
    k = max(imgTemp.eIndices (:,1));% k=0;
    for j= 1: BHeight % last to first %for j=1:BHeight % first to last
        x = imgTemp.eIndices (j,2);
        if x == i
            x = imgTemp.eIndices (j,1); %now save real value into x to find max value in array
            k = min(k,x); % k = max(k,x);
        end
    end
    imgTemp.newBMin(1,i) = k; % newBMax(1,i) = k;
end

% finally at the end of this function save the changes made to indivudal 
% image object
if(imgNum == 1)
    img.Cam1Img = imgTemp;
else
    img.Cam2Img = imgTemp;
end

end     %end of function