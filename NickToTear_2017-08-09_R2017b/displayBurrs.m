%%highlight burrs
% function displayDefects(cDefects, img, B)
function displayBurrs(img)
imgNum = 1;

% imshow(img.Img, [] );

for imgNum = 1:2
    if(imgNum == 1)
        imgTemp = img.Cam1Img;
        cDefects = imgTemp.endDefects;
    else
        imgTemp = img.Cam2Img;
        imgTemp.eIndices(:, 1) = imgTemp.eIndices(:, 1)+ DefectConstants.NEXT_IMG_ROW ;
        cDefects = imgTemp.burrDef;
    end
    
    [~,cCnt] = find(cDefects);
    % cDefSize= size(cDefects);
    % if isempty(cDefSize)
    %     return;
    % else
    %     cCnt = cDefSize(1,2);
    % end
    % startIndx = 1;
    % endIndx = 1;
    startIndx = 0;
    endIndx = 0;
    if isempty(cCnt)
        % %     now you use "getframe" and "frame2im"
        f = getframe(gca);
        im = frame2im(f);
        % save defect highlighted image in new folder
        filename = '1.png'; %default name
%         mkdir('Defects'); % create directory
%         filename = strcat('F:\GoodSamples\', img.currFilename);
%         imwrite(im,filename);
        % current strip doesnt have any strip
        break;
    else
        [~,colCnt] = size(cDefects);
        %time to highlight defect
%         figure
%         imshow(imgTemp.currImg,[]);
% % % %         title('Defect')
        hold on
        for indx = 1 :2: colCnt
            % Draw boundary line on defect edge
            %     plot(B(cDefects(1, 1): cDefects(1, 2),2),B(cDefects(1, 1): cDefects(1, 2),1),'g','LineWidth',2);
            if((indx+1) <= colCnt)
                if (cDefects(1, indx) > 0) && (cDefects(1, indx+1) > 0)
                    cstartIndx = cDefects(1, indx);
                    fprintf(img.fileID, '\n displayFinalDefects::Draw boundary line on defect cstartIndxedge %d ', cstartIndx);
                    disp(cstartIndx);
                    [startIndx,~] = find(imgTemp.eIndices(:, 2) == cstartIndx);
                    
%                     % if this is 2nd image find the indexes w.r.t. complete
%                     % image
%                     if(imgNum == 2)
%                         
%                     end % end of if
                    if indx < colCnt
                        cendIndx = cDefects(1, indx+1);
                        [endIndx, ~] = find(imgTemp.eIndices(:, 2) == cendIndx);
                    else
                        break;
                    end
                end
            end
            %         if( (startIndx ~= 0 ) && (endIndx ~= 0))
            if( ~isempty(startIndx) && ~isempty(endIndx))
                if( (startIndx(1,1) ~= 0 ) && (endIndx(1,1) ~= 0))
                    plot(imgTemp.eIndices(startIndx(1,1):endIndx(1,1),2), imgTemp.eIndices(startIndx(1,1):endIndx(1,1),1),'c','LineWidth',2);
                    %reinitialize
                    startIndx = 0;
                    endIndx = 0;
                end
            end
            % Draw bounding box around region
            %rectangle('Position',200, 200, 50 , 50,'EdgeColor','w')
            %        rectangle('Position',[830, 240, 200 , 150],...
            % 	'Curvature',[0,0],...
            % 	'EdgeColor', 'r',...
            % 	'LineWidth', 3,...
            % 	'LineStyle','-')
            
        end % endof for
        %% if found display burrs
        [r, c] = find(imgTemp.burrDef);
        [~, colCnt] = size(c);
        if(~isempty(colCnt))
            %found burr indices
            for indx = 1 :2: colCnt
                % Draw boundary line on defect edge
                %     plot(B(cDefects(1, 1): cDefects(1, 2),2),B(cDefects(1, 1): cDefects(1, 2),1),'g','LineWidth',2);
                if((indx+1) <= colCnt)
                    if (cDefects(1, indx) > 0) && (imgTemp.burrDef(1, indx+1) > 0)
                        cstartIndx = imgTemp.burrDef(1, indx);
                        [startIndx,~] = find(imgTemp.eIndices(:, 2) == cstartIndx);
                        if indx < colCnt
                            cendIndx = imgTemp.burrDef(1, indx+1);
                            [endIndx, ~] = find(imgTemp.eIndices(:, 2) == cendIndx);
                        else
                            return;
                        end
                    end
                end
                
                [r1, c1] = size(startIndx);
                [r2, c2] = size(endIndx);
                if( (r1 >=2) && (r2 >= 2) )
                    if( (startIndx(2,1) > 0 ) && (endIndx(2,1) > 0))
                        if(startIndx(2,1) < endIndx(2,1))
                            plot(imgTemp.eIndices(startIndx(2,1):endIndx(2,1),2), imgTemp.eIndices(startIndx(2,1):endIndx(2,1),1),'g','LineWidth',2);
                            %reinitialize
                            startIndx = 0;
                            endIndx = 0;
                        end
                    else if (startIndx(2,1)> endIndx(2,1))
                            plot(imgTemp.eIndices(endIndx(2,1):startIndx(2,1),2), imgTemp.eIndices(endIndx(2,1):startIndx(2,1),1),'g','LineWidth',2);
                            %reinitialize
                            startIndx = 0;
                            endIndx = 0;
                        end
                    end
                end
            end   %end of for...
        end%end of if((~isempty(c))
        hold off
    end % end of if
end %end of for

% %     now you use "getframe" and "frame2im"
f = getframe(gca);
im = frame2im(f);

if ~isempty(cCnt)
% save this in defects folder if no defects folder exist create one
newfilename = strcpy('burr_',img.currFilename); %default name
filename = strcat('F:\Defects\Burr' , img.currFilename);
imwrite(im,filename);
end
end %end of function