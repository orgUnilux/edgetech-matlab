% This function takes defect point as input argument and then it converts
% it into defect start and end indices
% this function also finds the zero region found here. zero region is
% default defect here.
% function tempDefects = defectPointToRange(tempDefects, cDefects)
function tempDefects = defectPointToRange(~, cDefects)
%%define individual indices as range
% for instance cDefect() = 60; define this defect as 45 to 75

tempDefects = NaN;
[~,cCnt] = size(cDefects);
tempDefIndxStart = 1;
tempDefIndxEnd = 2;
for indx = 1 : cCnt
    if(cDefects(1,indx) > 0)
        tempDefIndxStart = (2*indx) - 1; % set start point for current defect
        tempDefIndxEnd = 2*indx;       % set end point for current defect
        tempDefects(1, tempDefIndxStart) =cDefects(1,indx)-15;
        tempDefects(1, tempDefIndxEnd) = cDefects(1,indx)+15;
        if tempDefects(1, tempDefIndxStart) < 0
            %if this value is set to negative reset it to be positive
            tempDefects(1, tempDefIndxStart) = 5;
        end
    else
        break;
    end
end % end of for
% if cCnt > 0
%     cCnt = cCnt+1;
%     [~,zeroCnt] = find(cDefects);
%     if ~isempty(zeroCnt)
%         tempDefIndxStart = (2*cCnt) - 1; % set start point for current defect
%         tempDefIndxEnd = 2*cCnt;       % set end point for current defect
%         %we have indices at cDefZerosIndices
%         tempDefects(1, tempDefIndxStart) = cDefZerosIndices(1,1);
%         tempDefects(1, tempDefIndxEnd) = cDefZerosIndices(1,2);
%     end
% end % end of if cCnt > 0

end %end of function