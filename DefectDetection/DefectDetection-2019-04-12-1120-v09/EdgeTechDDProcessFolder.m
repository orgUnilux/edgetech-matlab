function status = EdgeTechDDProcessFolder(config_struct)
% Process an entire folder full of images
% imageFolderPath - the folder with images to process 
% settingsFilePath - the settings file to use 
% resultsFolderPath - the folder for storing all results
% function status = DDProcessFolder(imageFolderPath, settingsFilePath, resultsFolderPath)
%%%%


fname = 'EdgeTechDDProcessFolder';
status = -1;

% I dont know why this is needed but sure, lets
% fclose('all') closes all open files. Specify the literal all as a character vector or a string scalar.
fclose('all');


ImageFolderPath = config_struct.paths.input_dir;
if ( ~isdir(ImageFolderPath) )
    logMsg('%s: Bad Image Folder Path: %s', fname, ImageFolderPath);
    status = 1; % set error
    return;
end


ResultsFolderPath = config_struct.paths.output_dir;



% create ResultsFolderPath if does not exist 
% not very clean since 'isdir' is not exactly what we want and we dont
% check for errors
if ( ~isdir(ResultsFolderPath) )
    mkdir(ResultsFolderPath); 
end


% DefectFolderPath = sprintf('%s\\%s', ResultsFolderPath, 'Defects');
DefectFolderPath = ResultsFolderPath;
if ( ~isdir(DefectFolderPath) )
    mkdir(DefectFolderPath); 
end


CSVResultFile = sprintf('%s\\%s', ResultsFolderPath, 'results.csv'); 


logFileName = "results.log";
logFilePath = sprintf('%s\\%s', ResultsFolderPath, logFileName);




% currentStat = DefectConstants.ERR ;
% previousStat = DefectConstants.ERR ;
% timerFlag = false; % onetime flag to start timer for the executing multiple times
% t = timer;
% t.StartDelay = 5;

%%open log file
% lfid = -1; %% log file default id
% filecnt = zeros([500 1],'int8');
% kman increased to process large datasets 
% filecnt = zeros([2000 1],'int8');

% formatOut = 'yyyy-mm-dd';
% fileSuffix = datestr(now,formatOut);
% logfilename = strcat('DD_',fileSuffix, '.log');
% logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
% logFullPath = strrep(logTempPath, settingFilename, logfilename);
% %logFullPath = strcat(logFullPath, 'DD_',date, '.log');
% %logfilename = strcat('DD_',date, '.log');
% lfid = fopen(logFullPath, 'w+');
%CurrFilePath = "1.png";

% build the list of files to process

try
    DirFiles = dir(ImageFolderPath);
    
    % filecnt(:) = 0;
    
    total_files = 0;
    total_defective_files = 0;
    
    % first, count the .png files in folder
    total_image_files = 0;
    for k=1:length(DirFiles)
        FileName = DirFiles(k).name;
        if (endsWith(FileName,'.PNG','IgnoreCase',true))
            total_image_files = total_image_files + 1;
            FileList(total_image_files) = string(FileName);
        end
    end
    
    logStatus('INCOUNT=%d\n', total_image_files);
    
catch ME
    logMsg('%s: exception: %s %s', fname, ME.identifier, ME.message);
    status = 2;
    return;
end


% process images one by one
% FileList is the array of strings each representing a .PNG file name

%set up PROGRESS indicaion 
logStatus('PROGRESS=[');



file_list_length = length(FileList);
default_result = struct('status',-1);
results_array = repmat( default_result, file_list_length, 1 );
try
    for k=1:file_list_length
        results_array(k).status = -1;
        results_array(k).bDefectDetected = false;
        results_array(k).input_file_name = FileList(k);
        results_array(k).input_file_path = sprintf('%s\\%s', ImageFolderPath, results_array(k).input_file_name);
        results_array(k).defect_file_name = '';
        results_array(k).defect_file_path = ''; 
        total_files = total_files + 1;
        
    end
catch ME
    logMsg('%s: exception: %s %s', fname, ME.identifier, ME.message);
    status = 2;
    return;
end
   


status_file_name = config_struct.paths.status_file;

logMsg('%s: starting parfor loop for %d images', fname, file_list_length);

%for k=1:file_list_length
parfor k=1:file_list_length
    
    
        %reinit logging for parfor because logMsg is using globals and globals do not work with parfor
        logMsgInitDir(ProgramConstants.DEFAULT_LOG_FILE_DIR);
        
        %reinit status file for parfor because it uses globals
        logStatusInit(status_file_name, false);
        
        logStatus('+');
        

    try
        logMsg('%s: [k = %d] calling KCrackMainFunction(%s)', fname, k, results_array(k).input_file_path);
        results_array(k) = KCrackMainFunction(results_array(k).input_file_path, results_array(k).input_file_name, DefectFolderPath, CSVResultFile, config_struct, results_array(k));
        if (results_array(k).status == 0)
            logMsg('%s: [k = %d] KCrackMainFunction(%s) returned bDefectDetected = %d', fname, k, results_array(k).input_file_path, results_array(k).bDefectDetected);
        else
            logMsg('%s: [k = %d] KCrackMainFunction(%s) failed: status = %d', fname, k, results_array(k).input_file_path, results_array(k).status);
        end
    catch ME
        logMsg('%s: exception in KCrackMainFunction: %s %s', fname, ME.identifier, ME.message);
        results_array(k).status = -1;
    end
    
    
    if (results_array(k).status == 0)
        if(results_array(k).bDefectDetected)
            
            logStatus('d'); %write 'd' if contains defects
        else
            % good (not defective files)
            logStatus('g'); %write 'g' (good) if no defects
        end
    else
        logStatus('e'); % 'e' is for Error
    end
    
end % for/parfor k=...
logMsg('%s: left parfor loop for %d images', fname, file_list_length);



try
    
    logStatus(']\n'); %close status indication
    
    total_defective_files = 0;
    total_error_files = 0;
    for k=1:file_list_length
        if (results_array(k).status == 0)
            if(results_array(k).bDefectDetected)
                total_defective_files = total_defective_files + 1;
            end
        else
            total_error_files = total_error_files + 1;
        end
    end
    
    
    logStatus('DEFCOUNT=%d\n', total_defective_files);
    logStatus('ERRCOUNT=%d\n', total_error_files);
    
    % all is well
    logMsg('%s: total .png files: %d, defective files: %d, error files: %d', fname, total_files, total_defective_files, total_error_files);
    
    status = 0;
    for k=1:file_list_length
        if (results_array(k).status == 0)
            if (results_array(k).bDefectDetected)
                logStatus('DFILE=%s\n', results_array(k).defect_file_path);
            end
        else
            logStatus('ERRFILE=%s\n', results_array(k).input_file_path);
            if (status == 0)
                % save first non-zero status
                status = results_array(k).status;
            end
        end
    end
    
    
    %close all deletes all figures whose handles are not hidden.
    % close all hidden deletes all figures including those with hidden handles.
    close all;
    
catch READANDPROCESSEXCEPTION
    % logmsg  = sprintf(" WRITE FAILED: %s \n ",READANDPROCESSEXCEPTION.message);
    % logmsg  = sprintf(" WRITE FAILED: %s %s \n ",READANDPROCESSEXCEPTION.message,READANDPROCESSEXCEPTION.stack(2,2));
    logMsg('%s: EXCEPTION: %s', fname, READANDPROCESSEXCEPTION.message);
    status = 3;
end % end of try/catch block

logMsg("%s: leave: status = %d", fname, status);

close all;


end %end of function

