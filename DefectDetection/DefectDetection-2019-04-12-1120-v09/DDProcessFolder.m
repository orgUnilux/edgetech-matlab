%% Process an entire folder full of images
% imageFolderPath - the folder with images to process 
% settingsFilePath - the settings file to use 
% resultsFolderPath - the folder for storing all results
function status = DDProcessFolder(imageFolderPath, settingsFilePath, resultsFolderPath)

fname = 'DDProcessFolder';
status = -1;

% I dont know why this is needed but sure, lets
% fclose('all') closes all open files. Specify the literal all as a character vector or a string scalar.
fclose('all');


ImageFolderPath = imageFolderPath;
ResultsFolderPath = resultsFolderPath;


% create ResultsFolderPath if does not exist 
% not very clean since 'isdir' is not exactly what we want and we dont
% check for errors
if ( ~isdir(ResultsFolderPath) )
    mkdir(ResultsFolderPath); 
end


% DefectFolderPath = sprintf('%s\\%s', ResultsFolderPath, 'Defects');
DefectFolderPath = ResultsFolderPath;
if ( ~isdir(DefectFolderPath) )
    mkdir(DefectFolderPath); 
end


CSVResultFile = sprintf('%s\\%s', ResultsFolderPath, 'results.csv'); 


logFileName = "results.log";
logFilePath = sprintf('%s\\%s', ResultsFolderPath, logFileName);




% currentStat = DefectConstants.ERR ;
% previousStat = DefectConstants.ERR ;
% timerFlag = false; % onetime flag to start timer for the executing multiple times
% t = timer;
% t.StartDelay = 5;

%%open log file
% lfid = -1; %% log file default id
% filecnt = zeros([500 1],'int8');
% kman increased to process large datasets 
% filecnt = zeros([2000 1],'int8');

% formatOut = 'yyyy-mm-dd';
% fileSuffix = datestr(now,formatOut);
% logfilename = strcat('DD_',fileSuffix, '.log');
% logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
% logFullPath = strrep(logTempPath, settingFilename, logfilename);
% %logFullPath = strcat(logFullPath, 'DD_',date, '.log');
% %logfilename = strcat('DD_',date, '.log');
% lfid = fopen(logFullPath, 'w+');
%CurrFilePath = "1.png";


logMsgInit(logFilePath);
logMsg('%s: opened logfile %s', fname, logFilePath);

% we have this while here so we can break out on error (this is not really a loop)
while (1) 

    try
        
        if ( ~isdir(ImageFolderPath) )
            logMsg('%s: Bad Image Folder Path: %s', fname, ImageFolderPath);
            break; % break out of the while (1) loop on error, leave status unchanged 
        end
        
        Files = dir(ImageFolderPath);
 
        % filecnt(:) = 0;
                
        total_files = 0;
        total_defected_files = 0;
        
        for k=1:length(Files)
            FileName = Files(k).name;
            if(contains(FileName,'.png'))
                FilePath = sprintf('%s\\%s', ImageFolderPath, FileName);
              
                %dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );
                logMsg('%s: calling KCrackMainFunction(%s)', fname, FilePath);
                total_files = total_files + 1; 
                dFlag = KCrackMainFunction(FilePath, FileName, DefectFolderPath, settingsFilePath, CSVResultFile);
                
                % KCrackMainFunction returns 0 when defects are detected,
                % non-zero on no defects 
                % mnemonic true means good (not defective) image 
                logMsg('%s: KCrackMainFunction(%s) returned %d', fname, FilePath, dFlag);
                
                %% for parallel loop save defect cnt status in an array
                if(~dFlag)
                    total_defected_files = total_defected_files + 1;
                else
                    % good (not defected files)
                end
            end
        end % for k=...
        
        % all is well
        logMsg('%s: total .png files: %d, defected files: %d', fname, total_files, total_defected_files);
        
        %close all deletes all figures whose handles are not hidden.
        % close all hidden deletes all figures including those with hidden handles.
        close all;
        
        status = 0;
        
        break;
        
    catch READANDPROCESSEXCEPTION
        % logmsg  = sprintf(" WRITE FAILED: %s \n ",READANDPROCESSEXCEPTION.message);
        % logmsg  = sprintf(" WRITE FAILED: %s %s \n ",READANDPROCESSEXCEPTION.message,READANDPROCESSEXCEPTION.stack(2,2));
        logMsg('%s: EXCEPTION: %s', fname, READANDPROCESSEXCEPTION.message);
    end % end of try/catch block
    
    % we get here if there is a problem
    close all;
    status = -1;
    break;
   
end % end of while (1) 
    
    
logMsg("%s: leave: status = %d", fname, status);

close all;

return;

end %end of function

