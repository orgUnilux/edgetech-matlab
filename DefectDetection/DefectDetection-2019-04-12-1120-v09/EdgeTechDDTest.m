function status = EdgeTechDDTest()
%EDGETECHDDTEST Summary of this function goes here
%   Detailed explanation goes here

% close all open files (for test /debug)
fclose('all')

if (true)
%test EdgeTechDD throwing exception
test_name = '-test_exception';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we dont expect EdgeTechDD to return at all
    status = EdgeTechDD('-test_exception', 'test error text :)');
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
catch ME
    % this is expected
    fprintf('test ''%s'': exception (expected): %s: %s\n', test_name, ME.identifier, ME.message);
end
fprintf('test ''%s'' PASSED\n', test_name);
end


if (true)
%test EdgeTechDD throwing error
test_name = '-test_error';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we dont expect EdgeTechDD to return at all
    status = EdgeTechDD('-test_error');
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
catch ME
    % this is expected
    fprintf('test ''%s'': exception (expected): %s: %s\n', test_name, ME.identifier, ME.message);
end
fprintf('test ''%s'' PASSED\n', test_name);
end

if (true)
%when called without args should exit with 1
% this is going to cause MATLAB editor to close and exit
test_name = 'no args';
fprintf('\n*** test ''%s'' ***\n', test_name);
if (ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
    try
        status = EdgeTechDD();
        if (status ~= 1)
            fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
            status = 1;
            return;
        else
            % test passed
        end
        
        
    catch ME
        % this is unexpected
        fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
        status = 1;
        return;
    end
    
    fprintf('test ''%s'' PASSED\n', test_name);
    
else % if (ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
    % the EdgeTechDD should exit(1), if it does not - it is a FAILURE
    try
        status = EdgeTechDD();
        fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
        status = 1;
        return;
        
    catch ME
        % this is unexpected
        fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
    end
    
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
end 




if (true)
%when called with '-version' - print version and exit with success
test_name = '-version';
fprintf('\n*** test ''%s'' ***\n', test_name);
if (ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
    try
        % it is expected that EdgeTechDD returns 0
        status = EdgeTechDD('-version');
        if (status ~= 0)
            fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
            status = 1;
            return;
        end
    catch ME
        % this is NOT expected
        fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
        status = 1;
        return;
    end
    fprintf('test ''%s'' PASSED\n', test_name);
end
else
    try
        % it is expected that EdgeTechDD exits with 0 and does not return
        status = EdgeTechDD('-version');
        fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
        status = 1;
        return;
    catch ME
        % this is NOT expected
        fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
        status = 1;
        return;
    end
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
end


if (false)
%when called with -cfg but no file - throw exception
test_name = 'cfg no file';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we dont expect EdgeTechDD to return at all
    status = EdgeTechDD('-cfg');
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
catch ME
    % this is expected
    fprintf('test ''%s'': exception (expected): %s: %s\n', test_name, ME.identifier, ME.message);
end
fprintf('test ''%s'' PASSED\n', test_name);
end



if (false)
%test when cfg file is missing - expected to throw exception
test_name = 'cfg missing file';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we dont expect EdgeTechDD to return at all
    status = EdgeTechDD('-cfg', 'EdgeTechDD_missing.cfg');
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
catch ME
    % this is expected
    fprintf('test ''%s'': exception (expected): %s: %s\n', test_name, ME.identifier, ME.message);
end
fprintf('test ''%s'' PASSED\n', test_name);
end


if (false)
%test EdgeTechDD dealing with invalid cfg file
test_name = 'bad cfg section';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we dont expect EdgeTechDD to return at all
    status = EdgeTechDD('-cfg', 'EdgeTechDDBad1.cfg');
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
catch ME
    % this is expected
    fprintf('test ''%s'': exception (expected): %s: %s\n', test_name, ME.identifier, ME.message);
end
fprintf('test ''%s'' PASSED\n', test_name);
end


if (false)
% test a good run
test_name = 'good run EdgeTechDDTest2.cfg (QuickTest)';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we expect EdgeTechDD to return 0
    status = EdgeTechDD('-cfg', 'EdgeTechDDTest2.cfg');
    if (status ~= 0)
        fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
        status = 1;
        return;
    end
catch ME
    % this is NOT expected
    fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
    status = 1;
    return;
end
fprintf('test ''%s'' PASSED\n', test_name);
end


if (false)
% test a good run
test_name = 'good run EdgeTechDDTest3.cfg';
fprintf('\n*** test ''%s'' ***\n', test_name);
try
    % we expect EdgeTechDD to return 0
    status = EdgeTechDD('-cfg', 'EdgeTechDDTest3.cfg');
    if (status ~= 0)
        fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
        status = 1;
        return;
    end
catch ME
    % this is NOT expected
    fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
    status = 1;
    return;
end
fprintf('test ''%s'' PASSED\n', test_name);
end

if (true)
    % test a 'problem' run with files that potentially throw exception
    test_name = 'problem run EdgeTechDDTest4.cfg';
    fprintf('\n*** test ''%s'' ***\n', test_name);
    if (ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
        try
            % we expect EdgeTechDD to return 0
            status = EdgeTechDD('-cfg', 'EdgeTechDDTest4.cfg');
            if (status ~= 0)
                fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
                status = 1;
                return;
            end
        catch ME
            % this is NOT expected
            fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
            status = 1;
            return;
        end
        fprintf('test ''%s'' PASSED\n', test_name);
    end
else
    % we expect EdgeTechDD to exit with 0
    try
        status = EdgeTechDD('-cfg', 'EdgeTechDDTest4.cfg');
        % not expecting to get here
        fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
        status = 1;
        return;
        
    catch ME
        % this is NOT expected
        fprintf('test ''%s'' FAILED: exception: %s: %s\n', test_name, ME.identifier, ME.message);
        status = 1;
        return;
    end
    fprintf('test ''%s'' FAILED: EdgeTechDD returned %d\n', test_name, status);
    status = 1;
    return;
end




status = 0;

end

