function status = EdgeTechDD( varargin )
%EDGETECHDD Summary of this function goes here
%   Detailed explanation goes here
%   we use 'error' to cause this app to exist with status (-1), otherwise
%   it exists with 0. The value of 'status' is ignored by MATLAB once the
%   app is compiled into an executable. 

fname = 'EdgeTechDD';
config_file_name = '';
status = -1;




%first thing we do on startup is to setup logging to write to default dir
logMsgInitDir(ProgramConstants.DEFAULT_LOG_FILE_DIR);
logMsg('%s: starting: version %s, date: %s', fname, ProgramConstants.VERSION_STRING, ProgramConstants.DATE_STRING);


%log the arguments 
argscat = '';

% fprintf('Total number of inputs (nargin) = %d\n', nargin);

nargs = length(varargin);
% fprintf('Inputs in varargin(%d):\n',nargs)
for i = 1:nargs
    % strcat is ignoring whitespace 
    %argscat = strcat(argscat, varargin{i}, {' '});
    argscat = [argscat, varargin{i}, ' '];
    logMsg('%s: arg %d of %d: %s', fname, i, nargs, varargin{i});
end

logMsg('%s: args: %s', fname, argscat); 


% if (nargs == 0) 
%     status = usage();
%     return;
% end


%parse the argumetns 
i = 1;
while i <= nargs
   
    if (strcmp(varargin{i}, '-cfg') == true)
        i = i + 1;
        if (i > nargs)
            % print error and return non-zero
            % setting 'status' seems to be ignored
            status = usage_error('argument -cfg requires config file name');
            return; % return is not necessary after 'usage_error' but just in case
        end
        
        % save config file name for future use
        config_file_name = varargin{i};
    elseif (strcmp(varargin{i}, '-version') == true)
        % print version and exit with status 0
        status = print_version(); 
        return; % print_version exits 
    elseif (strcmp(varargin{i}, '-test_exception') == true)
        % do something that throws an exception to see how our program will handle it 
        status = -666;
        some_variable = exception_causing_var;
        return; % return is not necessary but just in case
    elseif (strcmp(varargin{i}, '-test_error') == true)
        err.identifier = 'EdgeTechDD:testError';
        err.message = 'this test message was caused by -test_error parameter';
        status = -666;
        error(err);
        return; % return is not necessary after 'error' but just in case
    elseif (strcmp(varargin{i}, '-test_return') == true)
        %simply return status from main()
        status = -666;
        return;
    elseif (strcmp(varargin{i}, '-test_parfor') == true)
        status = test_parfor();
        return;
    else
        % print error and exit(1)
        error_message = sprintf('unexpected token ''%s''', varargin{i});
        status = usage_error(error_message);
        return;
    end
    
    i = i + 1;
    
end % while i <= nargs

% we have to have a config file to proceed 
if (isempty(config_file_name))
    % describe problem and exit
    status = usage_error('no config file provided (-cfg)');
    return;
end

% another way to test for empty string 
% if (config_file_name == "")  
%     status = usage();
% end


%debug
logMsg('parsing config file: %s', config_file_name);

[status, config_struct] = parse_config_file(config_file_name);

if (status ~= 0)
   error_message = sprintf('parse_config_file(%s) failed: %d', config_file_name, status);
   % report error and exit
   status = report_error(-2, 'EdgeTechDD:invConfig', error_message);
   return;
end


% sucessfully parsed config file - all systems go

% create "status" file
status_file_name = config_struct.paths.status_file;
status = logStatusInit(status_file_name, true); % true means delete old file and start new
if (status ~= 0) 
    % log error and return -1
   error_message = sprintf('logStatusInit(%s) failed: %d', status_file_name, status);
   status = report_error(-3, 'EdgeTechDD:errStatus', error_message);
   return; 
end


logStatus('VERSION=%s\n', ProgramConstants.VERSION_STRING);
logStatus('BUILD_DATE=%s\n', ProgramConstants.DATE_STRING);
logStatus('input_dir=%s\n', config_struct.paths.input_dir);
logStatus('output_dir=%s\n', config_struct.paths.output_dir);
logStatus('status_file=%s\n', config_struct.paths.status_file);
logStatus('crack_sensitivity=%d\n', config_struct.parameters.crack_sensitivity);
logStatus('burr_sensitivity=%d\n', config_struct.parameters.burr_sensitivity);

% log config file 
% dump_config(config_struct);


% call the function that will actually do the work
status = EdgeTechDDProcessFolder(config_struct);
logStatus('RC=%d\n', status);
if (status ~= 0)
    
   error_message = sprintf('EdgeTechDDProcessFolder failed: %d', status);
   % shutdown parpool to avoid a 'ding' if parpool was started
   shutdown_parpool();
   
   % print error and exit
   status = report_error(-4, 'EdgeTechDD:errProcessFolder', error_message);
   return; %not reached 
end


logMsg('EdgeTechDDProcessFolder returned %d, exiting', status);

% shutdown parpool to avoid a 'ding' if parpool was started
shutdown_parpool();
   
if (~ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
    exit(status);
else
    logMsg('%s: not exiting with %d because ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR is set', fname, status);
    return;
end

end % end of function status = EdgeTechDD( varargin )









function [status, config_struct] = parse_config_file(config_file_name)
    fname = 'parse_config_file';
    status = 0;
    config_struct = struct; % so that we dont throw an exception about unasigned outputs 
    
try
    config_struct = ini2struct(config_file_name);
catch ME
    logMsg('%s: exception while parsing ''%s''\n', fname, config_file_name);
    logMsg('%s: %s : ''%s''\n', fname, ME.identifier, ME.message);
    status = 1; 
    return;
end

% validate structure and dump parameters 
try 
    paths_struct = config_struct.paths;
catch ME
    logMsg('%s: config file ''%s'' is missing section [paths]', fname, config_file_name);
    status = 2; 
    return;
end

try
    input_dir = paths_struct.input_dir;
    logMsg('%s: paths.input_dir: ''%s''', fname, input_dir);   
catch ME
    logMsg('%s: config file ''%s'' section [paths] is missing ''input_dir''', fname, config_file_name);
    status = 2; 
    return;
end

try
    output_dir = paths_struct.output_dir;
    logMsg('%s: paths.output_dir: ''%s''', fname, output_dir);
catch ME
    logMsg('%s: config file ''%s'' section [paths] is missing ''output_dir''', fname, config_file_name);
    status = 2; 
    return;
end

try
    status_file = paths_struct.status_file;
    logMsg('%s: paths.status_file: ''%s''', fname, status_file);
catch ME
    logMsg('%s: config file ''%s'' section [paths] is missing ''status_file''', fname, config_file_name);
    status = 2; 
    return;
end
    

try 
    parameters_struct = config_struct.parameters;
catch ME
    logMsg('%s: config file ''%s'' is missing section [parameters]', fname, config_file_name);
    status = 2; 
    return;
end

try
    crack_sensitivity = parameters_struct.crack_sensitivity;
    logMsg('%s: parameters.crack_sensitivity: %d', fname, crack_sensitivity);   
catch ME
    logMsg('%s: config file ''%s'' section [parameters] is missing ''crack_sensitivity''', fname, config_file_name);
    status = 2; 
    return;
end

try
    burr_sensitivity = parameters_struct.burr_sensitivity;
    logMsg('%s: parameters.burr_sensitivity: %d', fname, burr_sensitivity);   
catch ME
    logMsg('%s: config file ''%s'' section [parameters] is missing ''burr_sensitivity''', fname, config_file_name);
    status = 2; 
    return;
end

    status = 0;
end
    
%print message, usage, and exit with 1  
function status = usage_error(message)
    fname = 'usage_error';
    status = 1;
    identifier = 'EdgeTechDD:invalidArgs';
    logMsg('ERROR:%s: %s', identifier, message);
    print_usage();
    % err.identifier = identifier;
    % err.message = message;
    % error(err);
    
    %ME = MException('MyComponent:noSuchVariable', 'Variable %s not found', 'foo');
    %throw(ME)
    
    %this makes the process to actually exit and set status code %errorlevel% 
    if (~ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
        exit(status); 
    else
        logMsg('%s: not exiting with %d because ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR is set', fname, status);
        return;
    end
end

function status = report_error(exit_status, identifier, message)
    fname = 'report_error';
    status = exit_status;
    logMsg('ERROR:%d:%s: %s', status, identifier, message);
    %err.identifier = identifier;
    %err.message = message;
    % error(err);
    
    %ME = MException('MyComponent:noSuchVariable', 'Variable %s not found', 'foo');
    %throw(ME)
    
    if (~ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
        exit(status);
    else
        logMsg('%s: not exiting with %d because ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR is set', fname, status);
        return;
    end
end


function status = test_parfor()
fname = 'test_parfor';
num_iterations = 10;

logMsg('%s: entering parfor loop for %d iterations', fname, num_iterations);
parfor i=1:num_iterations
    
    t = getCurrentTask();
    if (isempty(t))
        t.ID = 0;
    end
    
    logMsg('%s: i = %d, task ID = %d', fname, i, t.ID);
    
end


logMsg('%s: leaving parfor loop after %d iterations', fname, num_iterations);



status = shutdown_parpool();

exit(status);

end

function status = shutdown_parpool()
fname = 'shutdown_parpool'; 
status = 0;
logMsg('%s: shutting down parpool to avoid a beep', fname);
delete(gcp('nocreate'));
end


function status = print_version()
fname = 'print_version';
status = 0;

fprintf('EdgeTechDD version: %s %s\n', ProgramConstants.VERSION_STRING, ProgramConstants.DATE_STRING);
if (~ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR)
    exit(status);
else
    logMsg('%s: not exiting with %d because ProgramConstants.DEBUG_DO_NOT_EXIT_ON_ERROR is set', fname, status);
    return;
end

end

function dump_config(cfg)

logMsg('cfg.paths.input_dir = %s', cfg.paths.input_dir);

logMsg('cfg.paths.output_dir = %s', cfg.paths.output_dir);

end

function status = print_usage()
status = -1;
fprintf('EdgeTechDD usage:\n');
fprintf('EdgeTechDD -cfg <config>\n');


end
