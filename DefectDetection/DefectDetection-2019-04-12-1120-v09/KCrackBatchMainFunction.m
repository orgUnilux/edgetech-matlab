%% read images in iteration
%  script has few hard coded values based on the current EdgeTech camera 
% image standard size i.e. 1003 X 1296
% function dFlag = KCrackMainFunction( subFolderPath, CurrFileName, CurrFilePath, fileID )
% subFolderPath = 'D:\Images\Coils/2017_06_14_HB_TOFQR-LNXUQR/Knife1\\'
% CurrFileName = 'K_ZAQKP (1).png'
% CurrFilePath = 'D:\Images\Coils/2017_06_14_HB_TOFQR-LNXUQR/Knife1\\K_ZAQKP (1).png'
%KCrackMainFunction( 'D:\Images\Coils/2017_06_14_HB_TOFQR-LNXUQR/Knife1\\', 'D:\Images\Coils/2017_06_14_HB_TOFQR-LNXUQR/Knife1\\', 'D:\Images\Coils/2017_06_14_HB_TOFQR-LNXUQR/Knife1\\K_ZAQKP (1).png' )
% function dFlag = KCrackMainFunction( subFolderPath, CurrFileName, CurrFilePath )
function [img dFlag] = KCrackBatchMainFunction( I, im)
% Filenames = zeros(10000, 2);
% Files=dir('*.*'); %commented for findImagesFiles
filecnt = 0;
Ext = '.png';
%im = KnifeBatchFolderFiles(  );

if(isempty(im))
 return;
end
dFlag = false;
%FileName = ;
CurrFilePath = im;
newsplit = strsplit(im, {'\\','\','/'});
[~,cnew_split_size] = size(newsplit);

 if(cnew_split_size)
    CurrFileName =  newsplit(cnew_split_size);

 else
     return ;
 end
newsplit{1,cnew_split_size} = ''; %delete filename from this string
subFolderPath = strjoin(newsplit, {'\\'}); 

endDefects = zeros(1,200, 'int16');
ImgFile = zeros(1,2);

bTwoLayers = false;

%if this is a folder
folder = strfind(CurrFilePath, '.');

ImgFile = strfind(CurrFilePath,'.png');
disp('...........');
disp(CurrFilePath);

[m,~] = size(ImgFile);

ImgH3File = strfind(CurrFilePath,'Defect');
[m3,~] = size(ImgH3File);
ImgH4File = strfind(CurrFilePath,'GoodSample');
[m4,~] = size(ImgH4File);

 if((m > 0) && (m3 == 0) && (m4 == 0))            %testing all the png files in current folder

    [r, c, ch] = size(I);
    if ch == 3
        %find if this is rgb or gray image
        % if rgb convert to grayscale
        newI = rgb2gray(I);
        %I = 0;
        I = newI;
    end %end of if ch==3
   
    img = imageInfo(I, CurrFileName, CurrFilePath);    
    %% read and apply from the csv text file input
    img = readTextFileInput(img);       
    img = applyTextFileInput(img);
     %% split into 2 images
    n=fix(size(I,1)/2);
    
for bstrip = 0:1:1      
    if(~bstrip)
    A1=I(1:n-1,:,:);
  
    close all;  % close all windows
    %   determine if type 1 and 2 defects are eligible on this image
    img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting
    if((img.Cam1Img.imgType == 1) || (img.Cam1Img.imgType == 2))
        img = AdaptMethod(img, img.Cam1Img.imgNumber);   % extract strip and find uneven edge
       img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting

        img = UnevenEdge(img, img.Cam1Img.imgNumber);
        %             disp('unevenDef');
        %tempDefects1 = findDefects(newBMin, endDefects);
        [img, img.Cam1Img.unevenDef] = findDefects(img, img.Cam1Img.imgNumber);

        % binary area analysis
        % find below average defects
        newBArea = binAreaAnalysis(img.Cam1Img.IBin);       
        img.Cam1Img.belowAvgDef = findBelowAvgDef(img, img.Cam1Img.imgNumber, newBArea);
        
        img = findBurr(img, img.Cam1Img.imgNumber);        
    end
 
    if((img.Cam1Img.imgType == 1) || (img.Cam1Img.imgType == 2))
        img.Cam1Img.endDefects = [img.Cam1Img.unevenDef img.Cam1Img.belowAvgDef img.Cam1Img.burrDef ];
        img.Cam1Img.endDefects( :, ~any(img.Cam1Img.endDefects,1) ) = [];  %remove all columns with zeros
        
   end
    else
    A2=I(n+3:end,:,:);
   close all;  % close all windows
 %determine if type 1 and 2 defects are eligible on this image
   img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
    if((img.Cam2Img.imgType == 1) || (img.Cam2Img.imgType == 2))
         img = AdaptMethod(img, img.Cam2Img.imgNumber);     % extract strip and find uneven edge
         %determine if type 1 and 2 defects are eligible on this image
        img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
      
          %find uneven edge
        img = UnevenEdge(img, img.Cam2Img.imgNumber);
         [img, img.Cam2Img.unevenDef] = findDefects(img, img.Cam2Img.imgNumber);
        
        %binary area analysis
        newBArea = binAreaAnalysis(img.Cam2Img.IBin);
        %             disp('findBelowAvgDef');
        img.Cam2Img.belowAvgDef = findBelowAvgDef(img, img.Cam2Img.imgNumber, newBArea);
        
        %             displayDefects(img, img.Cam2Img.imgNumber, img.Cam2Img.belowAvgDef);
        
        img = findBurr(img, img.Cam2Img.imgNumber);
 
    end
    if((img.Cam2Img.imgType == 1) || (img.Cam2Img.imgType == 2))
        img.Cam2Img.endDefects = [img.Cam2Img.unevenDef img.Cam2Img.belowAvgDef img.Cam2Img.burrDef];
        img.Cam2Img.endDefects( :, ~any(img.Cam2Img.endDefects,1) ) = [];  %remove all columns with zeros
        
    end
    
    close all;
    dFlag = false;
    dFlag = CheckFinalDefects(img, subFolderPath);
%     if(dFlag)
%       %% if defect found make directory for defects
%       using_strrep = strrep(CurrFilePath, 'Knife', 'Defects');
%       SplitPathArr = strsplit(using_strrep, Ext);
%       SplitPath = SplitPathArr(1);
%       
%       if(~isdir(SplitPath))
%           mkdir(SplitPath);
%       end
%       
%       SplitPathDefArr = strsplit(CurrFilePath, 'Defects');
%       SplitPathDef = SplitPathDefArr(2);
%     end
 end %end of if bstrip 1 or 2
 end % end of parfor loop  
end % end of if (strcmp(...

end %end of function
% disp('End of KnifeCrackMainFunction');

