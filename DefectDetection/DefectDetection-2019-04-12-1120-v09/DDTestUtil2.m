function status = DDTestUtil2(  )

% path to where images to be processed are; no slash at the end
ImageFolderPath = 'D:\\Projects\\TestSets\\Test1\\Images';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test1\\Images\Defects';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test1\\config';

ImageFolderPath = 'D:\\Projects\\TestSets\\Test2\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test2\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test2\\config_min\Defects';

ImageFolderPath = 'D:\\Projects\\TestSets\\Test2\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test2\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test2\\config_max\Defects';

% burrs from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test3\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test3\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test3\\config_min\Defects';
% log: total files: 10, defected files: 10

% burrs from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test3\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test3\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test3\\config_max\Defects';
% log: total files: 10, defected files: 2



% knife kracks from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test4\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test4\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test4\\config_min\Defects';

% knife kracks from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test4\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test4\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test4\\config_max\Defects';

% 'other' from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test5\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test5\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test5\\config_min\Defects';

% 'other' from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test5\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test5\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test5\\config_max\Defects';


% 'wearing knives' from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test6\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test6\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test6\\config_min\Defects';

% 'wearing knives' from Matts' HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test6\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test6\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test6\\config_max\Defects';

% 'Defective edge trim examples' from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test7\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test7\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test7\\config_min\Defects';
% log: defect files count : 24

% 'Defective edge trim examples' from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test7\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test7\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test7\\config_max\Defects';
% log: defect files count : 10

% 'Bad edges' from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test8\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test8\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test8\\config_min\Defects';
% did not work as all the files '.jpg'

% 'Defective trims' from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test9\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test9\\config_min';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test9\\config_min\Defects';
% log: total files: 1123, defected files: 617

% 'Defective trims' from Matt's HD 
ImageFolderPath = 'D:\\Projects\\TestSets\\Test9\\Images';
ConfigFolderPath = 'D:\\Projects\\TestSets\\Test9\\config_max';
DefectFolderPath = 'D:\\Projects\\TestSets\\Test9\\config_max\Defects';
% log: total files: 1123, defected files: 238

% 'Rejects'
ImageFolderPath = 'D:\Projects\TestSets\Test10\Images';
ConfigFolderPath = 'D:\Projects\TestSets\Test10\config_min';


% DefectFolderPath = 'D:\Projects\TestSets\Test10\config_min\Defects';
% CSVResultFile    = 'D:\Projects\TestSets\Test10\config_min\results.csv';
% log: total files: 23, defected files: 6





% ------------------


settingsFileName = 'DefectSettings.csv';
settingsFilePath = sprintf('%s\\%s', ConfigFolderPath, settingsFileName);  


ResultsFolderPath = sprintf('%s\\%s', ConfigFolderPath, 'Results');

% create ResultsFolderPath if does not exist 
% not very clean since 'isdir' is not exactly what we want and we dont
% check for errors
if ( ~isdir(ResultsFolderPath) )
    mkdir(ResultsFolderPath); 
end


DefectFolderPath = sprintf('%s\\%s', ResultsFolderPath, 'Defects');
if ( ~isdir(DefectFolderPath) )
    mkdir(DefectFolderPath); 
end


CSVResultFile = sprintf('%s\\%s', ResultsFolderPath, 'results.csv'); 


logFileName = "DDTestUtil.log";
logFilePath = sprintf('%s\\%s', ResultsFolderPath, logFileName);



% the meaningfull stuff starts here
% status is what we return
status = -1;

% I dont know why this is needed but sure
fclose('all');


% currentStat = DefectConstants.ERR ;
% previousStat = DefectConstants.ERR ;
% timerFlag = false; % onetime flag to start timer for the executing multiple times
% t = timer;
% t.StartDelay = 5;

%%open log file
% lfid = -1; %% log file default id
% filecnt = zeros([500 1],'int8');
% kman increased to process large datasets 
filecnt = zeros([2000 1],'int8');

% formatOut = 'yyyy-mm-dd';
% fileSuffix = datestr(now,formatOut);
% logfilename = strcat('DD_',fileSuffix, '.log');
% logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
% logFullPath = strrep(logTempPath, settingFilename, logfilename);
% %logFullPath = strcat(logFullPath, 'DD_',date, '.log');
% %logfilename = strcat('DD_',date, '.log');
% lfid = fopen(logFullPath, 'w+');
%CurrFilePath = "1.png";



%logmsg  = sprintf(" DefectDetection log file open \n ");
%LogMessage(logmsg);

logMsgInit(logFilePath);
logMsg('DDTestUtil: opened logfile %s', logFilePath);


%% wait in forever loop for edgetech to update the FKC link

while (1) % we have this while here so we can break out on error 

    try
        % %         logmsg = sprintf(" Config Test from file at %s \n ",fullpath);
        % %         logmsg = LogMessage( logmsg);
        
        
        if ( ~isdir(ImageFolderPath) )
            logMsg("Bad Image Folder Path: %s", ImageFolderPath);
            break; % break out of the while (1) loop on error
        end
        
        Files = dir(ImageFolderPath);
        
        dFlag = false;
        filecnt(:) = 0;
                
        
 %      parfor k=1:length(Files)
        total_files = 0;
        for k=1:length(Files)
            FileName = Files(k).name;
            if(contains(FileName,'.png'))
                FilePath = sprintf('%s\\%s', ImageFolderPath, FileName);
              
                %dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );
                logMsg("calling KCrackMainFunction(%s)", FilePath);
                total_files = total_files + 1; 
                dFlag = KCrackMainFunction(FilePath, FileName, DefectFolderPath, settingsFilePath, CSVResultFile);
                
                logMsg("KCrackMainFunction(%s) returned %d", FilePath, dFlag);
                
                %% for parallel loop save defect cnt status in an array
                if(~dFlag)
                    filecnt(k) = 1;
                else
                    filecnt(k) = 0;
                end
            end
        end % for k=...
       
        
        defCnt = sum(filecnt(:));
        
        logMsg('total files: %d, defected files: %d', total_files, defCnt);
        
        close all;
    catch READANDPROCESSEXCEPTION
        logmsg  = sprintf(" WRITE FAILED: %s \n ",READANDPROCESSEXCEPTION.message);
        %logmsg  = sprintf(" WRITE FAILED: %s %s \n ",READANDPROCESSEXCEPTION.message,READANDPROCESSEXCEPTION.stack(2,2));
        logmsg = LogMessage( logmsg);
        
    end % end of try/catch block
    
    % all is well
    status = 0;
    break; % we are not really running in a loop; the while (1) loop is bogus
   

end % end of while 
    
    
logMsg("DDTestUtil: leave: status = %d", status);

close all;

return;

end %end of function

