%% this function reads the tet file input
% text file has sensitivity parameters for
% image defect detection - crakc and burr

%%function start
function img = readTextFileInput(img, settingsFileName)
logMsg('readTextFileInput: enter: settingsFileName = %s', settingsFileName);
%function readTextFileInput()

% img.readInFull = csvread('DefectSettings.csv');
% img.txtCrackVals = csvread('DefectSettings.csv',1,1,[1 1 1 3]);
% img.txtBurrVals =  csvread('DefectSettings.csv',2,1,[2 1 2 3]);
% img.txtDefCnt = csvread('DefectSettings.csv',3,1,[3 1 3 3]);

% 
% img.readInFull = csvread('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv');
% img.txtCrackVals = csvread('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv',1,1,[1 1 1 3]);
% img.txtBurrVals =  csvread('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv',2,1,[2 1 2 3]);
% img.txtDefCnt = csvread('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv',3,1,[3 1 3 3]);


% % img.readInFull = csvread('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv');
% % img.txtCrackVals = csvread('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv',1,1,[1 1 1 3]);
% % img.txtBurrVals =  csvread('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv',2,1,[2 1 2 3]);
% % img.txtDefCnt = csvread('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv',3,1,[3 1 3 3]);


% choose the folder for defect setings file
% Program Files (x86) or Program Files 
% path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
% path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
% settingFilename = 'DefectSettings.csv';

% if(isdir(path_64b))
%     fullpath = strcat(path_64b, settingFilename);    
% else
%     fullpath = strcat(path_32b, settingFilename);   
% end

img.readInFull = csvread(settingsFileName);
img.txtCrackVals = csvread(settingsFileName,1,1,[1 1 1 3]);
img.txtBurrVals =  csvread(settingsFileName,2,1,[2 1 2 3]);
img.txtDefCnt = csvread(settingsFileName,3,1,[3 1 3 3]);

logMsg('readTextFileInput: leave: settingsFileName = %s', settingsFileName);
end % end of function