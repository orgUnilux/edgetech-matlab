%% this function pens file nameed'DD_yyyy-mm-dd.log'
% logs thje  'message' in the file
% closes the file after logging
function message = logMsg(format, varargin)
fname = 'logMsg';
global logMsgLogFileName;
global logMsgLogFileDir;

message = sprintf(format, varargin{:});



% construct log file name

if (~isempty(logMsgLogFileDir))
    % if logMsgLogFileDir is specified construct log file name based on date
    date = datestr(now,ProgramConstants.DEFAULT_LOG_FILE_DATE_FORMAT);
    logFileName = sprintf(ProgramConstants.DEFAULT_LOG_FILE_FORMAT, date);
    logFilePath = sprintf('%s\\%s', logMsgLogFileDir, logFileName);
else
    logFilePath = logMsgLogFileName;
end

%timestamp = datestr(datetime('now'), 'yyyy-mm-dd HH:mm:ss');
timestamp = datestr(datetime('now'), 'yyyy-mm-dd HH:MM:SS');
t = getCurrentTask(); 
if (isempty(t)) 
   t.ID = 0; 
end

try
    lfid = fopen(logFilePath, 'a+');
    %lfid = fopen("goo.log", 'a+');
    if(lfid > 0)
        fprintf(lfid, '%s:[%d]: %s\n', timestamp, t.ID, message);
        fclose(lfid);
    end
catch ME
    %fprintf('%s: exception: %s %s\n', fname, ME.identifier, ME.message);
end

% with no file id, fpritnf is an equivalent for disp(sprintf())
fprintf('%s:[%d]: %s\n', timestamp, t.ID, message); 




return; 


%%open log file
lfid = -1; %% log file default id
% choose the folder for defect setings file
% Program Files (x86) or Program Files
ConfigPath_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
ConfigPath_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'DefectConfig.txt';

if(isdir(ConfigPath_64b))
    fullpath = strcat(ConfigPath_64b, settingFilename);
else
    fullpath = strcat(ConfigPath_32b, settingFilename);
end

settingFilename = 'DefectConfig.txt';
formatOut = 'yyyy-mm-dd';
fileSuffix = datestr(now,formatOut);
logfilename = strcat('DD_',fileSuffix, '.log');
logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
logFullPath = strrep(logTempPath, settingFilename, logfilename);
%logFullPath = strcat(logFullPath, 'DD_',date, '.log');
%logfilename = strcat('DD_',date, '.log');
lfid = fopen(logFullPath, 'a+');
if(lfid > 0)
 temp = message;
 
%%get date time string array
 t = datestr(datetime('now','TimeZone','local','Format','MM/yyyy//dd HH:mm:ss'));
 %%write logg message to file
log = sprintf("%s EdgeTech::%s\n", t, message);
fwrite(lfid, log);
% now that we wrote successfully to the log file delete the string
erase(message,temp); 
rstat = fclose(lfid);
end % end  of if
end %end of function