function findKnifeFolderFiles(  )

% path to where images to be processed are; no slash at the end
ImageFolderPath = "D:\Projects\TestSets\Test1\Images";

% same as ImageFolderPath but with a slash 
KnifeFolderPath = strcat(path,'\\');



fclose('all');
% choose the folder for defect setings file
% Program Files (x86) or Program Files
% ConfigPath_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
ConfigPath_64b = "D:\Projects\TestSets\Test1\config\";

ConfigPath_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
ConfigFilename = 'DefectConfig.txt';
settingFilename = 'DefectSettings.csv';

if(isdir(ConfigPath_64b))
    fullpath = strcat(ConfigPath_64b, ConfigFilename);
    sfullpath = strcat(ConfigPath_64b, settingFilename);
else
    fullpath = strcat(ConfigPath_32b, ConfigFilename);
    sfullpath = strcat(ConfigPath_32b, settingFilename);
end

currentStat = DefectConstants.ERR ;
previousStat = DefectConstants.ERR ;
% timerFlag = false; % onetime flag to start timer for the executing multiple times
% t = timer;
% t.StartDelay = 5;

%%open log file
% lfid = -1; %% log file default id
filecnt = zeros([500 1],'int8');
% formatOut = 'yyyy-mm-dd';
% fileSuffix = datestr(now,formatOut);
% logfilename = strcat('DD_',fileSuffix, '.log');
% logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
% logFullPath = strrep(logTempPath, settingFilename, logfilename);
% %logFullPath = strcat(logFullPath, 'DD_',date, '.log');
% %logfilename = strcat('DD_',date, '.log');
% lfid = fopen(logFullPath, 'w+');
%CurrFilePath = "1.png";
logmsg  = sprintf(" DefectDetection log file open \n ");
logmsg = LogMessage( logmsg);

%% wait in forever loop for edgetech to update the FKC link

while (1) % we have this while here so we can break out on error 

    try
        % %         logmsg = sprintf(" Config Test from file at %s \n ",fullpath);
        % %         logmsg = LogMessage( logmsg);
        
        
        if ( ~isdir(ImageFolderPath) )
            logmsg  = sprintf("  knife folder path  not valid: %s \n ", ImageFolderPath);
            logmsg = LogMessage( logmsg);
            break; % break out of the while (1) loop on error
        end
        
        
        %% we found path in text file continue with algorithm
        logmsg  = sprintf("  path read from file %s \n ", ImageFolderPath);
        logmsg = LogMessage( logmsg);
        
        Files=dir(ImageFolderPath);
        
        
        Files=dir(FolderPath);
        
        dFlag = false;
        filecnt(:) = 0;
                
        
 %      parfor k=1:length(Files)
        for k=1:length(Files)
            FileName = Files(k).name;
            if(contains(FileName,'.png'))
                FilePath = strcat(KnifeFolderPath, FileName);
                dFlag = false;
                %dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );
                dFlag = KCrackMainFunction(KnifeFolderPath, FileName, FilePath);
                %% for parallel loop save defect cnt status in an array
                if(~dFlag)
                    filecnt(k) = 1;
                else
                    filecnt(k) = 0;
                end
            end
        end % for k=...
       
        
        defCnt = sum(filecnt(:));
        logmsg  = sprintf(' defect files count : %d', defCnt);
        LogMessage( logmsg);
        
        close all;
    catch READANDPROCESSEXCEPTION
        logmsg  = sprintf(" WRITE FAILED: %s \n ",READANDPROCESSEXCEPTION.message);
        %logmsg  = sprintf(" WRITE FAILED: %s %s \n ",READANDPROCESSEXCEPTION.message,READANDPROCESSEXCEPTION.stack(2,2));
        logmsg = LogMessage( logmsg);
        %delete(t);
    end % end of try/catch block

end % end of while 
    
    
logmsg  = sprintf(" Sucessfully closed application: \n ");
LogMessage( logmsg);

close all;

end %end of function