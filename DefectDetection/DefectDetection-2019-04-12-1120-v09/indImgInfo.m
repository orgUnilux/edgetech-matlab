classdef indImgInfo
    % IndImageinfo - class for individual image credentials
    
    % Properties
    properties
        currImg
        iWidth        %image width in pixels
        iHeight       %image height in pixels
        iFullImgRef
        
        imgNumber     % in current file strip number 1 or 2
        alignedCamImg   % strip aligned horizontally
        aWidth           %aligned image width in pixels
        aHeight          %aligned image height in pixels
        bAligned         %is the image aligned?(true/false)
        RC               % 3 indices of the strip(nick tear and transition)
        
        SmoothedImage    % smoothed image (applied guass filter)
        IBin             % binary image format old variable BWEdge
        IBinNick         % binary Nick region of strip
        IBinNoFill       %is not filled region in adapt method(used for transition regions analysis)
        TransNickBin     % binary Nick region for image with transition
        TransTearBin     % binary Tear region for image with transition
        % Image types:
        % 0 - no image
        % 1 - no nicktear contrast
        % 2 - nick to tear contrast
        % 3 - transition
        imgType        % type of image 
        imgTypeDetails % details for imgType
        
        % kman to save results of the first and second pass of
        % 'imageAnalysis'
        imgType1        % type of image 
        imgTypeDetails1 % details for imgType
        
        imgType2        % type of image 
        imgTypeDetails2 % details for imgType
        
        
        
        Save            % row projection of the image
        SaveDeriv       % derivative of row projection
        SCave            % column projection of the image
        SCaveDeriv       % derivative of column projection
        eIndices        % indices of strip boundary
        newBMin
        newBMax
        newNickBMin
        
        % defect indices from different alfgorithms
        unevenDef      % uneven algo defects
        belowAvgDef
        burrDef
        endDefects
        
        % total counts for the above
        unevenDefCount
        belowAvgDefCount
        burrDefCount
        endDefCount
        
        
        
        
        % the ones below may be unused
        highlightedDef
        nickRegionDef
        nickBelowAvgDef
        nickAboveAvgDef
        
    
        
        bSaveDefImg    % flag to see if the image is gud for saving it to the defects folder
        
    end % end of properties    

    % Methods
    methods
        function imgInfo = indImgInfo(imgNum)
%             imgInfo.currImg = img;   
            imgInfo.imgNumber = imgNum;
            imgTemp.bSaveDefImg = false;
        end % end of imageInfo constructor
        
        function in = Set_Defaults(in)
            if ~isempty(in.currImg)
                [r, c] = size(in.currImg);
                in.iWidth = c;
                in.iHeight = r;
            else
                %no input image
                return;
            end
            
            in.alignedCamImg = 0;            
            in.aWidth = 0;
            in.aHeight = 0;
            
            in.IBin = 0;
            in.IBinNick = 0;
            in.IBinNoFill = 0;
            in.TransNickBin = 0;
            in.TransTearBin = 0;            
            in.RC = [0 0 0];
            in.imgType = 0;
            in.imgTypeDetails = '';
            
            % kman initialize to some special value so that we can tell
            % if these fields were ever assigned any values
            in.imgType1 = -1;
            in.imgTypeDetails1 = '?';
            
            in.imgType2 = -1;
            in.imgTypeDetails2 = '?';
            
            in.Save = 0;
            in.SaveDeriv = 0;
            in.eIndices = 0;            
        end
    end % end of methods
end % end of class


