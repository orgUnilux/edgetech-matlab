    %% read specific file
    I = imread('Unilux-20170515_1042.png');
    %figure, imshow(I);
    %         img = Set_Defaults(img);
    [r, c, ch] = size(I);
    if ch == 3
        %find if this is rgb or gray image
        % if rgb convert to grayscale
        newI = rgb2gray(I);
        %I = 0;
        I = newI;
    end %end of if ch==3
%     %img = imageInfo(I, CurrFileName, CurrFilePath, fileID);
%     %img = imageInfo(I, CurrFileName, CurrFilePath);
%     
%     %% read and apply from the csv text file input
%     img = readTextFileInput(img);       
%     img = applyTextFileInput(img);
    
    %fprintf(img.fileID, '\n KCrackMainFunction::..................... %s', CurrFilePath);
    %fprintf(img.fileID, '\n filecnt::..................... %d', filecnt);
    %% split into 2 images
    n=fix(size(I,1)/2);
    A1=I(1:n-1,:,:);
    
    A11 = imcomplement(A1);
    figure, imshow(A11, []);
    
    L = watershed(A11);
    figure, imshow(L,[]);
    
    A11i = imhmin(A11, 40);
    figure, imshow( A11i);
    