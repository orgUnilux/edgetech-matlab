classdef ProgramConstants
    %ProgramConstants Program Flow switches and debug flag
    %   Detailed explanation goes here
    
    properties (Constant = true)
        VERSION_STRING = '0.9';
        DATE_STRING = '2019-04-12';
        DEFAULT_LOG_FILE_DIR = 'C:\Program Files (x86)\Unilux\EdgeTech\logs\Defect_logs';
        DEFAULT_LOG_FILE_DATE_FORMAT = 'yyyy-mm-dd';
        DEFAULT_LOG_FILE_FORMAT = 'DD_%s.log';
        DEFAULT_STATUS_FILE = 'C:\Program Files (x86)\Unilux\EdgeTech\Temp\EdgeTechDD.status';
        
        
        
        % DEBUG_SHOW_IMAGES = true; 
        DEBUG_SHOW_IMAGES = false;
        DEBUG_DO_NOT_EXIT_ON_ERROR = false;
    end
    
    methods
    end
    
end

