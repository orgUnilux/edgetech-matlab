
function B = binAreaAnalysis(BW)
%%Method#1 this algorithm finds missing cut region
binary_area = sum(BW,1);
% plot (binary_area);
B = binary_area;
end %end of function
