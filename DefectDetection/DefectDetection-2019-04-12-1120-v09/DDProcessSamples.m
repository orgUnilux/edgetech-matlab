

function status = DDProcessSamples(samplesFolderPath, settingsFilePath, resultsFolderPath)

fname = 'DDProcessFolder';

status = -1;

fclose('all');


logFilePath = sprintf('%s\\%s.log', resultsFolderPath, fname);

logMsgInit(logFilePath);
logMsg('%s: opened logfile %s', fname, logFilePath);


% we have this while here so we can break out on error (this is not really a loop)
while (1) 

    try
        
       
        
        if ( ~isdir(samplesFolderPath) )
            logMsg('%s: Bad Image Folder Path: %s', fname, samplesFolderPath);
            break; % break out of the while (1) loop on error, leave status unchanged 
        end
        
        % created resultsFolderPath if does not exist 
        if ( ~isdir(resultsFolderPath) )
            mkdir(resultsFolderPath); 
        end
        
        Folders = dir(samplesFolderPath);
        Folders = Folders(~ismember({Folders.name},{'.','..'}));
        Folders = Folders([Folders.isdir]);
        
        
        status = 0;        
        total_folders = 0;
        
        for k = 1:length(Folders)
            FolderName = Folders(k).name;
            
            logMsgInit(logFilePath);
            logMsg('%s: %s', fname, FolderName);
            
            imageFolderPath = sprintf('%s\\%s', samplesFolderPath, FolderName);
            resultsSubfolderPath = sprintf('%s\\%s', resultsFolderPath, FolderName);
            
            status = DDProcessFolder(imageFolderPath, settingsFilePath, resultsSubfolderPath);

            logMsgInit(logFilePath);
            logMsg('%s: DDProcessFolder(%s) returned %d', fname, FolderName, status);
            
            if (status ~= 0)
                break;
            end
            
            total_folders = total_folders + 1;
        end % for k=...
        
        % all is well?
        logMsgInit(logFilePath);
        if (status ~= 0) 
            logMsg('%s: ERROR: only %d out of %d folders processed', fname, total_folders, length(Folders));
        else
            
            logMsg('%s: SUCCESS: %d out of %d folders processed', fname, total_folders, length(Folders));
        end
        
        %close all deletes all figures whose handles are not hidden.
        % close all hidden deletes all figures including those with hidden handles.
        close all;
        
        break;
        
    catch READANDPROCESSEXCEPTION
        % logmsg  = sprintf(" WRITE FAILED: %s \n ",READANDPROCESSEXCEPTION.message);
        % logmsg  = sprintf(" WRITE FAILED: %s %s \n ",READANDPROCESSEXCEPTION.message,READANDPROCESSEXCEPTION.stack(2,2));
        logMsg('%s: EXCEPTION: %s', fname, READANDPROCESSEXCEPTION.message);
    end % end of try/catch block
    
    % we get here if there is a problem
    close all;
    status = -1;
    break;
   
end % end of while (1) 
    
    
logMsg("%s: leave: status = %d", fname, status);

close all;

return;

end %end of function




