%%Check final Defects
% This function checks if the image is having defects. This is used by
% batch processing ( returns true if  defects)
% derived from function displayDefects(cDefects, img, B)
function defFlag = CheckFinalDefects(img, FolderPath)

% following 2 flags are used to determine if the strip  
% on the whole image is a good sample,used for iteration
bStrip1GoodSample = false;
bStrip2GoodSample = false; 
imgNum = 1;
 %imshow(img.Img, [] );
defFlag = false;
%%%f = figure('visible', 'off');
fh = figure;
fh.Visible = 'off';

% highlight = img.Img;

for imgNum = 1:2
    if(imgNum == 1)
        imgTemp = img.Cam1Img;
        cDefects = imgTemp.endDefects;
    else
        imgTemp = img.Cam2Img;        
        imgTemp.eIndices(:, 1) = imgTemp.eIndices(:, 1)+ DefectConstants.NEXT_IMG_ROW;
        cDefects = imgTemp.endDefects;
    end
    
    [~,cCnt] = find(cDefects);
    % cDefSize= size(cDefects);
    % if isempty(cDefSize)
    %     return;
    % else
    %     cCnt = cDefSize(1,2);
    % end
    % startIndx = 1;
    % endIndx = 1;
    startIndx = 0;
    endIndx = 0;
    if isempty(cCnt)
        if(imgNum == 2)
            bStrip2GoodSample = true;      
        else
            bStrip1GoodSample = true;
        end


    end % end of if
end %end of for
 if( (bStrip1GoodSample && bStrip2GoodSample) )
     defFlag = true;
 end
end %end of function