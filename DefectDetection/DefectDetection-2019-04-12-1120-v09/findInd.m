%% 2016-06-21 find eIndices with different method
% when bwtraceboundary cannot find indices of the strip border, this method
% is used to find the near possible indices.
function eIndices = findInd( Bin )
% Bin = imread('newBin.png');
[r,c] = find(Bin > 0);
eIndicesTop = zeros([1296 2]);
eIndicesBottom = zeros([1296 2]);

for i = 1 : 1296
    [newr, ~] = find(c(:) == i);
%     newval = r(newr(:));
    if(~isempty(newr))
        % save row values in a new array
        newval = r(newr(:));
        %find min(top strip index)
        eIndicesTop(i,1) = min(newval);
        eIndicesTop(i,2) = i;
        %find max(bottom strip index)
        eIndicesBottom(i,1) = max(newval);
        eIndicesBottom(i,2) = i;
    end
end

if (ProgramConstants.DEBUG_SHOW_IMAGES)
    % imshow(Bin);
    hold on;
    plot(eIndicesTop(:,2), eIndicesTop(:,1),'g','LineWidth',2);
    plot(eIndicesBottom(:,2), eIndicesBottom(:,1),'g','LineWidth',2);
    hold off;
end

% reverse the bottom border indices
eIndBottomRev = eIndicesBottom(end:-1:1,:);
eIndices = vertcat(eIndicesTop, eIndBottomRev); % reverse the bottom indices

end % end of function
