%% alignStripHorz: this function extracts binary image. traces the boundary
% then depending on the orientation property shifts the image to align the
% strip horizontal(0 degrees)
% is the image aligned? or not? return as flag
% function [I_out] = alignStripHorz(I)
function [I_out, flag] = alignStripHorz(I)
logMsg('alignStripHorz: enter');
I_out = I;  %default image in output
flag = false;
% [A11_uni, cutBinary] = adaptBlocks(I); %method #1 to handle non-uniform illumination
[A11_uni, ~] = adaptBlocks(I); %method #1 to handle non-uniform illumination

% imsegmenter(I);

propArea = regionprops(A11_uni, {'Area'});
propOrient = regionprops(A11_uni, {'Orientation'});
areas = cat(1, propArea.Area);
orients = cat(1, propOrient.Orientation);
[rOrient, cOrient] = size(orients);

maxArea = max(areas);

[r,c] = find( areas >= maxArea);

if (isempty(r) || isempty(c))
    %% strip doesnot exists
    logMsg('alignStripHorz: leave: EVENT: strip doesnot exists');
    return;
end



if((r <= rOrient) && (c <= cOrient))
    StripAngle = orients(r,c);
end

if(StripAngle > 1) || (StripAngle < -1)
   %rotate original image
   StripAngle = StripAngle * (-1);
   I_out = imrotate(I, StripAngle);
   flag = true;
end

logMsg('alignStripHorz: leave: Rotated image %f degrees', StripAngle);

end % end of function