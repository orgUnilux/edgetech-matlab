function img = findBurr(img, imgNum)
 cDefects = zeros([1 300]);
 
cnt = 1; %defect index
if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end
% [imHeight, imWidth] = size(BWEdge);
[BHeight, ~] = size(imgTemp.eIndices );
imgTemp.newBMax = zeros(size(imgTemp.IBin(1,:)));
%B matrix has first column as pixel value
% 2nd column as the y coordinate
% extract values till image width (here 1296)
for i = 1 : imgTemp.iWidth
%     k = max(imgTemp.eIndices (:,1));
    k=0;
    for j= 1: BHeight % last to first %for j=1:BHeight % first to last
        x = imgTemp.eIndices (j,2);
        if x == i
            x = imgTemp.eIndices (j,1); %now save real value into x to find max value in array
%             k = min(k,x); 
            k = max(k,x);
        end
    end
    imgTemp.newBMax(1,i) = k; % newBMax(1,i) = k;
end

%plot(imgTemp.newBMax);
%hold on;
%% in a for loop read the avg for a block of values then determine the burr 
% based on that region
for i = 1 : imgTemp.iWidth
    startIndx = ((i - 1) * 108) + 1;
    endIndx = startIndx + 107; % block size is 108

%     newBAvg = mean(imgTemp.newBMax);
    newBAvg = mean(imgTemp.newBMax(startIndx : endIndx));
    % introduce threshold for the  average value
%     newBAvg = newBAvg + 5;
    newBAvg = newBAvg + img.burr_avg_threshold;
    
%     aveall(1, 1:imgTemp.iWidth) = newBAvg;
    aveall(1, startIndx:endIndx) = newBAvg;
    %plot(aveall); %now plot and find the defect
    
    %now find the difference between average and the border values
    diffVals = imgTemp.newBMax(startIndx:endIndx) - aveall(1, startIndx:endIndx);
    % if any of the diff values are greater than 0 they are considered as defects
    [rowDiff, colDiff] = find(diffVals > 0);
    if ~isempty(colDiff)
        cDefects(1,cnt) = startIndx + colDiff(1,1);
        cDefects(1,(cnt+1)) = startIndx + colDiff(1,end);
        cnt = cnt + 2;
        
        imgTemp.bSaveDefImg = true;
    end
    
    if(endIndx >= imgTemp.iWidth)
        break;
    end    
end % end of for i =...
%hold off;

[~, c] = find(cDefects > 0);
if(~isempty(c))
imgTemp.burrDef = cDefects;
end

% finally at the end of this function save the changes made to indivudal 
% image object
if(imgNum == 1)
    img.Cam1Img = imgTemp;
else
    img.Cam2Img = imgTemp;
end

end %end of function findBurr