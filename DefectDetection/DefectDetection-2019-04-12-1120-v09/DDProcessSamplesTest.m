function status = DDProcessSamplesTest()

samplesFolderPath = 'D:\Defect Detection Samples\Steel samples AMNS Calvert Small Subset\Images\Samples';
settingsFilePath = 'D:\Defect Detection Samples\Steel samples AMNS Calvert Small Subset\Images\config_min\DefectSettings.csv';
resultsFolderPath = 'D:\Defect Detection Samples\Steel samples AMNS Calvert Small Subset\Images\Samples_min2';


status = DDProcessSamples(samplesFolderPath, settingsFilePath, resultsFolderPath);


end

