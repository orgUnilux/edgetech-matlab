%%Display Defects
% function displayDefects(cDefects, img, B)
% kman 2018-11-07:
% FolderPath -> defectFolderPath
% returns 'true' - defects detected, 'false' - no defects found
function result_struct_out = displayFinalDefects(img, defectFolderPath, result_struct)
fname = 'displayFinalDefects'; 

logMsg('%s: enter: defectFolderPath = %s', fname, defectFolderPath);

result_struct_out = result_struct;
result_struct_out.status = -1;


% following 2 flags are used to determine if the strip
% on the whole image is a good sample,used for iteration
bStrip1GoodSample = false;
bStrip2GoodSample = false;
imgNum = 1;
%imshow(img.Img, [] );
result_struct_out.bDefectDetected = false;
%%%f = figure('visible', 'off');


% highlight = img.Img;



%imgNum = 1
imgTemp1 = img.Cam1Img;
cDefects1 = imgTemp1.endDefects;
[~,cCnt1] = find(cDefects1);
if isempty(cCnt1)
   bStrip1GoodSample = true; 
else 
   bStrip1GoodSample = false; 
end

%imgNum = 2
imgTemp2 = img.Cam2Img;
imgTemp2.eIndices(:, 1) = imgTemp2.eIndices(:, 1)+ DefectConstants.NEXT_IMG_ROW;
cDefects2 = imgTemp2.endDefects;
[~,cCnt2] = find(cDefects2);
if isempty(cCnt2)
   bStrip2GoodSample = true; 
else 
   bStrip2GoodSample = false; 
end


if (bStrip1GoodSample && bStrip2GoodSample)
     result_struct_out.bDefectDetected = false;
     result_struct_out.status = 0;
     logMsg('%s: leave: bDefectDetected = %d', fname, result_struct_out.bDefectDetected);
     return;
end

result_struct_out.bDefectDetected = true;

% we get here only if some defects were detected
img.ImgRGB = repmat(img.Img, 1, 1, 3); % equivalent to cat(3, img.Img, img.Img, img.Img)

if (false)
    fh = figure;
    fh.Visible = 'off';
    
    hFig = gcf;
    hAx  = gca;
    
    % hide the toolbar
    set(hFig,'menubar','none');
    
    % make figure window fullscreen
    %set(hFig,'units','pixels','outerposition',[0 0 1920 1180]);
    set(hFig,'units','normalized','outerposition',[0 0 1 1]);
    
    [height, width] = size(img.Img);
    % the '+1' magic makes the saved images to be of a correct size
    % this maybe something to do that in recent versions of
    % MATLAB the pixel size is not a pixel but 1/96 of an inch.
    set(hAx,'Unit','pixels','Position',[0 0 width+1 height+1]);
    
    % to hide the title
    % set(hFig,'NumberTitle','off');
    
    imshow(img.Img, 'InitialMagnification', 100);
    %x = [0 1200];
    %y = [0 1200];
    %image(img.Img);
    %set(gca,'units','pixels');
    % pos = get(gca,'position');
    
    %# display the top left part of the image at magnification 100%
    % xlim([0.5 pos(3)-0.5]),ylim([0.5 pos(4)-0.5]);
    hold on
end



            


for imgNum = 1:2
    if (imgNum == 1) 
        imgTemp = imgTemp1;
        cDefects = cDefects1;
        cCnt = cCnt1;
    else
        imgTemp = imgTemp2;
        cDefects = cDefects2;
        cCnt = cCnt2;
    end
    
     startIndx = 0;
     endIndx = 0;
     
    [~,colCnt] = size(cDefects);
    for indx = 1:2:colCnt
        
        % Draw boundary line on defect edge
        %     plot(B(cDefects(1, 1): cDefects(1, 2),2),B(cDefects(1, 1): cDefects(1, 2),1),'g','LineWidth',2);
        if((indx+1) <= colCnt)
            if (cDefects(1, indx) > 0) && (cDefects(1, indx+1) > 0)
                cstartIndx = cDefects(1, indx);
                %fprintf(img.fileID, '\n displayFinalDefects::Draw boundary line on defect cstartIndxedge %d ', cstartIndx);
                % kman 2018-11-07 disp(cstartIndx);
                [startIndx,~] = find(imgTemp.eIndices(:, 2) == cstartIndx);
                
                %                     % if this is 2nd image find the indexes w.r.t. complete
                %                     % image
                %                     if(imgNum == 2)
                %
                %                     end % end of if
                if indx < colCnt
                    cendIndx = cDefects(1, indx+1);
                    [endIndx, ~] = find(imgTemp.eIndices(:, 2) == cendIndx);
                else
                    break;
                end
            end
        end
        %         if( (startIndx ~= 0 ) && (endIndx ~= 0))
        if( ~isempty(startIndx) && ~isempty(endIndx))
            if( (startIndx(1,1) ~= 0 ) && (endIndx(1,1) ~= 0))
                %eIndices is a 2592 x 2 matrix (2 columns 2592 rows)
                plotX = imgTemp.eIndices(startIndx(1,1):endIndx(1,1),2);
                plotY = imgTemp.eIndices(startIndx(1,1):endIndx(1,1),1);
                % plotX and plotY are Nx1 matrixes (column) seemingly
                % of consecutive numbers in plotX - need to make sure.
                %logMsg('%s: plotX = %d, plotY = %d', fname, plotX, plotY);
                % 'y' is for yellow
                if (false)
                    plot(plotX, plotY,'y','LineWidth',2);
                end
                
                [height, width, depth] = size(img.ImgRGB);
                for i = 1:length(plotX)
                    X = plotX(i);
                    Y = plotY(i);
                    
                    if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                        img.ImgRGB(Y, X, 1) = 255;
                        img.ImgRGB(Y, X, 2) = 255;
                        img.ImgRGB(Y, X, 3) = 0;
                    end
                 
                    Y = Y + 1;
                    if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                        img.ImgRGB(Y, X, 1) = 255;
                        img.ImgRGB(Y, X, 2) = 255;
                        img.ImgRGB(Y, X, 3) = 0;
                    end
                end
                %[low, high] = findLowHigh(imgTemp.eIndices, startIndx(1,1))
                % highlight(imgTemp.eIndices(startIndx(1,1):endIndx(1,1),2), imgTemp.eIndices(startIndx(1,1):endIndx(1,1),1)) = 255;
                
                
                %reinitialize
                startIndx = 0;
                endIndx = 0;
            end
        end
        % Draw bounding box around region
        %rectangle('Position',200, 200, 50 , 50,'EdgeColor','w')
        %        rectangle('Position',[830, 240, 200 , 150],...
        % 	'Curvature',[0,0],...
        % 	'EdgeColor', 'r',...
        % 	'LineWidth', 3,...
        % 	'LineStyle','-')
        
    end %for indx = 1:2:colCnt
    
    
    %% if found display burrs
    [r, c] = find(imgTemp.burrDef);
    [~, colCnt] = size(c);
    %if(~isempty(colCnt))      % kman XXX BUG? when buffDef has all zeroes, colCnt is not 'empty' it is just 0 indicating that c is 'empty'
    if (~isempty(c))
        %found burr indices
        for indx = 1 :2: colCnt
            % Draw boundary line on defect edge
            %     plot(B(cDefects(1, 1): cDefects(1, 2),2),B(cDefects(1, 1): cDefects(1, 2),1),'g','LineWidth',2);
            if((indx+1) <= colCnt)
                if (cDefects(1, indx) > 0) && (imgTemp.burrDef(1, indx+1) > 0)
                    cstartIndx = imgTemp.burrDef(1, indx);
                    [startIndx,~] = find(imgTemp.eIndices(:, 2) == cstartIndx);
                    if indx < colCnt
                        cendIndx = imgTemp.burrDef(1, indx+1);
                        [endIndx, ~] = find(imgTemp.eIndices(:, 2) == cendIndx);
                    else
                        logMsg('%s:leave: line=123: ERROR: bDefectDetected = %d', fname, result_struct_out.bDefectDetected);
                        result_struct_out.status = -1;
                        return;
                    end
                end
            end
            
            [r1, c1] = size(startIndx);
            [r2, c2] = size(endIndx);
            if( (r1 >=2) && (r2 >= 2) )
                if( (startIndx(2,1) > 0 ) && (endIndx(2,1) > 0))
                    if(startIndx(2,1) < endIndx(2,1))
                        plotX = imgTemp.eIndices(startIndx(2,1):endIndx(2,1),2);
                        plotY = imgTemp.eIndices(startIndx(2,1):endIndx(2,1),1);
                        
                        if (false)
                            plot(plotX, plotY , 'r','LineWidth',2);
                        end
                        
                        [height, width, depth] = size(img.ImgRGB);
                        for i = 1:length(plotX)
                            X = plotX(i);
                            Y = plotY(i);
                            
                            if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                                img.ImgRGB(Y, X, 1) = 255;
                                img.ImgRGB(Y, X, 2) = 0;
                                img.ImgRGB(Y, X, 3) = 0;
                            end
                            
                            Y = Y + 1;
                            if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                                img.ImgRGB(Y, X, 1) = 255;
                                img.ImgRGB(Y, X, 2) = 0;
                                img.ImgRGB(Y, X, 3) = 0;
                            end
                            
                        end
                        
                        
                        
                        % highlight(imgTemp.eIndices(startIndx(2,1):endIndx(2,1),2), imgTemp.eIndices(startIndx(2,1):endIndx(2,1),1)) = 255;
                        
                        %reinitialize
                        startIndx = 0;
                        endIndx = 0;
                        
                    elseif (startIndx(2,1) > endIndx(2,1))
                        plotX = imgTemp.eIndices(endIndx(2,1):startIndx(2,1),2);
                        plotY = imgTemp.eIndices(endIndx(2,1):startIndx(2,1),1);
                        
                        if (false)
                            plot(plotX, plotY, 'r','LineWidth', 2);
                        end
                        
                        [height, width, depth] = size(img.ImgRGB);
                        
                        for i = 1:length(plotX)
                            X = plotX(i);
                            Y = plotY(i);
                           
                            if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                                img.ImgRGB(Y, X, 1) = 255;
                                img.ImgRGB(Y, X, 2) = 0;
                                img.ImgRGB(Y, X, 3) = 0;
                            else
                                logMsg('%s: indx = %d, i = %d, X = %d, Y = %d', fname, indx, i, X, Y);
                            end
                            
                            Y = Y + 1;
                            if ((X > 0) && (Y > 0) && (Y <= height) && (X <= width))
                                img.ImgRGB(Y, X, 1) = 255;
                                img.ImgRGB(Y, X, 2) = 0;
                                img.ImgRGB(Y, X, 3) = 0;
                            end
                            
                        end
                        
                        
                        % highlight(imgTemp.eIndices(endIndx(2,1):startIndx(2,1),2), imgTemp.eIndices(endIndx(2,1):startIndx(2,1),1)) = 255;
                        
                        
                        %reinitialize
                        startIndx = 0;
                        endIndx = 0;
                    end
                end
            end
        end % if(~isempty(colCnt)) 
        
    end %for indx = 1:2:colCnt
end %for imgNum = 1:2
        
     


          

    




%%     now you use "getframe" and "frame2im"

if (false)
    hold off
    
    imfr = getframe(gca);
    im = frame2im(imfr);
    
    %imwrite(im, defectFilePath);
end


%FolderPath (includes \\) is where the procesed image lives
%img.currFilename is processed image file name

if (~isdir(defectFolderPath))
    mkdir(defectFolderPath);
end

defectFilePath = sprintf('%s\\%s', defectFolderPath, img.currFilename);
result_struct_out.defect_file_name = img.currFilename;
result_struct_out.defect_file_path = defectFilePath;

logMsg('%s: writing defect file %s', fname, defectFilePath);

%kman 2019-03-27
imwrite(img.ImgRGB, defectFilePath);


if (0)
    
    defStr = strrep(FolderPath,'Knife', 'Defect');
    %   defStr = strcat(defStr,'\\');
    defpath = sprintf('%s\\',defStr);
    kFilenametmp = img.currFilename;
    dFilenametmp = strrep(kFilenametmp, 'K_', 'D_');    % rename image filename prefix 'K_' as 'D_'
    %dFileTmpMat = cell2mat(dFilenametmp); %%convert cell to mat
    %dfilename = sprintf('%s%s',defpath , dFileTmpMat);
    dfilename = strcat(defStr , dFilenametmp);
    bDefectDetected = true;
    if(~isdir(defpath))
        mkdir( defpath );
    end %end of if
    imwrite(im,dfilename);
    
    %imwrite(highlight,filename);
end
    
result_struct_out.status = 0;

logMsg('displayFinalDefects: leave: bDefectDetected = %d', result_struct_out.bDefectDetected);
%  imwrite(im,filename);

end %end of function