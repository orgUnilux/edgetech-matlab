function [low, high] = findLowHigh(eIndices, val)

a = find(eIndices(:,2) == val);

b = min(a);
c = max(a);

low = eIndices(b,1);
high = eIndices(c,1);

end % end of function