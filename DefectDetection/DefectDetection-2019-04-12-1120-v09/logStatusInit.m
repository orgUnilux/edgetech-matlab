function status = logStatusInit( logStatusName, bDeleteOldFile )
% logStatusInit - Initialize status file


global logStatusFileName;
logStatusFileName = logStatusName;
if (bDeleteOldFile)
    status = delete_old_status_file(logStatusFileName);
else
    status = 0;
end



end


function status = delete_old_status_file(filename)
fname = 'delete_old_status_file'; 


try
    if (exist(filename, 'file') == 2)
        % File exists.
        logMsg('%s: status file ''%s'' exists, deleting.', fname, filename);
        delete(filename);
        if (exist(filename, 'file') == 2)
            logMsg('%s: unable to delete status file ''%s''.', fname, filename);
            status = 1;
            return;
        else
            
        end
        
    else
        % File does not exist.
    end
    
catch ME
    logMsg('%s(%s) exception: %s: %s', fname, filename, ME.identifier, ME.message);
    status = 2; 
    return;
end

status = 0;


end