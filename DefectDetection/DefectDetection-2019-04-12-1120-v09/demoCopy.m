%%
% min curr and max in this function arguments are read from the csv file
%%
function demoCopy(path, minFolderCnt, currFolderCnt, maxFolderCnt)
%function demoCopy(path, currFolderCnt)
Smpl = path; % give some default path
%% condition to check if this is demonstration or realworld
if (currFolderCnt && (currFolderCnt  <= maxFolderCnt))
    %if (currFolderCnt)
    %Smpl = strings(currFolderCnt,1);
    currFolderStr = num2str(currFolderCnt);
    % folderCnt will tell if it is 1 2 or 3
    if(contains( path, 'D:\'))
        
        Smpl = 'D:\\DemoSmplsCopy\\Knife';
        
    else
        if(contains( path, 'F:\'))
            Smpl = 'F:\\DemoSmplsCopy\\Knife';
        end
    end
    
    Smpl = strcat(Smpl,currFolderStr);
    
    Files=dir(path);
    %knifePath = strcat(path,'\\');
    
    %% delelte png files
    dFlag = false;
    filecnt = 0;
    for k=1:length(Files)
        CurrName = Files(k).name;
        if(contains(CurrName,'.png'))
            oldpath =  cd(path);
            delete *.png;   % delete all the images in knife folder
            cd(oldpath);
            break; % deleted all files now exit
        end  % end of if
    end
    
    %% delete folder
    %[newsplit, ~] = split(path,["\","\\"]);
    
    %[r,c] = size(newsplit);
    %vals = contains(newsplit, 'Knife');
    %knifeFolderName = newsplit(r,c);
    
    %[noKnifePath , ~] = split(path, 'Knife');
    
    %     oldpath = cd(noKnifePath);
    %     rmdir Knife1;
    %     cd(oldpath);
    %rmdir path;
    %rmdir D:/Images/Coils/2017_03_13_HB_ZPPLJ-JXNRNB/Knife1;
    %newKnifePathStr = strcat(noKnifePath(1,1),'Knife', currFolderStr);
    
    %mkdir newKnifePathStr
    %copyfile(Smpl, noKnifePath(1,1));
    
    Files=dir(Smpl);
    %knifePath = strcat(path,'\\');
    
    %% read an write png files

    for k=1:length(Files)
        currFolder = Files(k).folder;
        CurrName = Files(k).name;
        if(contains(CurrName,'.png'))
            % read file from the defect samples path
             oldfilePath = strcat(currFolder, '\', CurrName);
            currImg = imread(oldfilePath);
            
            %if no K_ prefix exists , name it with K_ prefix 
            flg = contains(CurrName,'K_');
            if(~flg)
                newFilename = strcat('K_',CurrName);
            else
                newFilename = CurrName;
            end
            %format file  name including path
            newfilePath = strcat(path, newFilename);
            
            imwrite(currImg, newfilePath);
        end
        
    end % end of for
    
end % end of if folderCnt
end % end of function