%% this function reads the tet file input
% text file has sensitivity parameters for
% image defect detection - crakc and burr

%%function start
%function applyTextFileInput()

function img = applyTextFileInput(img)
logMsg('applyTextFileInput: enter');

img.below_thresh = img.txtCrackVals(2);
img.burr_avg_threshold = img.txtBurrVals(2);
logMsg('applyTextFileInput: img.below_thresh = %d, img.burr_avg_threshold = %d', img.below_thresh, img.burr_avg_threshold);

% % 
% % %code testing
% % filecnt = 4;
% % img.readInFull(4,3) = filecnt;
% % 
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', img.readInFull);
% % %csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', img.txtDefCnt, 3, 2 );

logMsg('applyTextFileInput: leave');
end % end of function