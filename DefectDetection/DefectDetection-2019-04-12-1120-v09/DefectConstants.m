classdef DefectConstants
    properties (Constant = true)
        BURR_AVG_THRESHOLD = 5; % findBurr.m adjust burr
        ADAPT_BLK_WIDTH = 108; % adaptBlocks.m
        BELOW_THRESH = 10; % findBelowAvgDef.m used to adjust knife crack
        MAX_AVG_PIXEL_VAL = 50; % max avg pixel value for defect detection
        
        %%
        MAX_DEFECTS_CNT = 298;
        NEXT_IMG_ROW = 503; % hard coded, this is the column number where 
                                    % the camera2 image starts
        % IND_IMG_ROW_TOP_THESH and IND_IMG_ROW_BTM_THESH are btoth
        % hardcoded values at the time of applying algo on the image
        % BELOW_THRESH will be checked with these values.
        IND_IMG_ROW_TOP_THESH = 10;
        IND_IMG_ROW_BTM_THESH = 490;
        
        %% sensitivity minimum and maximum values
        % kman 2018-11-17 - these do not appear to be used
        MAX_KC = 15; % max threshold value to detect knife crack
        MIN_KC = 5; % min threshold value to detect knife crack
        MAX_BURR = 15; % max burr
        MIN_BURR = 5;   % min burr sensitivity
        
        %%
        %Coordination for ET and DD applicaitons by using the following
        % status indicators
        ERR = 0
        PROC = 1;
        WAIT = 2;
        %RECOV = 3;
    end
end % end of classdef DefectConstants