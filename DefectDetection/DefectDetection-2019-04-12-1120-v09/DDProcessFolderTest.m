%% DDProcessFolderTest()
%  Simple test rig to test DDProcessFolder
function status = DDProcessFolderTest()

imageFolderPath = 'D:\ImageTestSets\00-SmokeTest\Images';
settingsFilePath = 'D:\ImageTestSets\00-SmokeTest\config_min\DefectSettings.csv';
resultsFolderPath = 'D:\ImageTestSets\00-SmokeTest\Results1';

status = DDProcessFolder(imageFolderPath, settingsFilePath, resultsFolderPath);

% imageFolderPath = 'D:\Projects\TestSets\Test10\Rejects';
% settingsFilePath = 'D:\Projects\TestSets\Test10\config_min\DefectSettings.csv';
% resultsFolderPath = 'D:\Projects\TestSets\Test10\Results2';
%
% status = DDProcessFolder(imageFolderPath, settingsFilePath, resultsFolderPath);


end

