function parKnifeFolderFiles(  )

% choose the folder for defect setings file
% Program Files (x86) or Program Files
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'DefectConfig.txt';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);
else
    fullpath = strcat(path_32b, settingFilename);
end

disp("from file at ");
disp(fullpath);
fileid = fopen(fullpath, 'r');
path = fscanf(fileid, '%s');   % read path written by edgetech
fclose(fileid);

disp("path read from file " );
disp(path);

Files=dir(path);
knifePath = strcat(path,'\\');

[minVal, currSmplVal, maxVal] = writeTextFileInput();
if( minVal && currSmplVal && maxVal)
    % if settings file has values for Defect Samples folder
    demoCopy(knifePath, minVal, currSmplVal, maxVal);
end
Files=dir(path);

dFlag = false;
% filecnt = 0;
parfor k=1:length(Files)
    CurrName = Files(k).name;
    if(strfind(CurrName,'.png'))
        
        CurrFilePath = strcat(knifePath, CurrName);
        
        dFlag = false;
        %dFlag = KCrackMainFunction(subFolderPath, currSubFile, CurrFilePath, fileID );
        dFlag = KCrackMainFunction(knifePath, CurrName, CurrFilePath );
%         if(dFlag)
%             filecnt = filecnt  +1;
%         end
%         disp(' defect files count : ');
%         disp(filecnt);
    end
end % for k=...



% choose the folder for defect setings file
% Program Files (x86) or Program Files
path_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
path_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'DefectSettings.csv';

if(isdir(path_64b))
    fullpath = strcat(path_64b, settingFilename);
else
    fullpath = strcat(path_32b, settingFilename);
end
% readInFull = csvread(fullpath);
% readInFull(4,3) = filecnt;
% csvwrite(fullpath, readInFull); % write settings to the file
% % %
% % readInFull(4,3) = filecnt;
% % csvwrite('C:\Program Files\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('C:\Program Files (x86)\Unilux\EdgeTech\config\DefectSettings.csv', readInFull);
% % %csvwrite('DefectSettings.csv', readInFull);
close all;



end %end of function