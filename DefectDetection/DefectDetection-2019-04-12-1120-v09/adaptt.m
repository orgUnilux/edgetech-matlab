function [seg] = adaptt(x)

thresh = multithresh(x, 2);
seg = imquantize(x,thresh);
% properties = regionprops(seg, {'Area', 'Eccentricity', 'EquivDiameter', 'EulerNumber', 'MajorAxisLength', 'MinorAxisLength', 'Orientation', 'Perimeter'});

%     threshVal = graythresh(x);
%     BW_Out  = im2bw(x, threshVal);    
%     % Get properties.
%     properties = regionprops(BW_out, {'Area', 'Eccentricity', 'EquivDiameter', 'EulerNumber', 'MajorAxisLength', 'MinorAxisLength', 'Orientation', 'Perimeter'});
% 
%     % Uncomment the following line to return the properties in a table.
%     % properties = struct2table(properties);


end