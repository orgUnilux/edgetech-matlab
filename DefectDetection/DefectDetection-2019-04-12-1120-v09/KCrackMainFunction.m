%% read images in iteration
%  script has few hard coded values based on the current EdgeTech camera
% image standard size i.e. 1003 X 1296


% kman 2018-11-07 modified this function to make incompatible because I
% changed the order and the names of the parameters
% CurrFilePath -> imageFilePath - full path to the image to process, e. g.
% C:\MyFiles\foo.png
% CurrFileName -> imageFileName - image file name to process, e. g. foo.png
% subFolderPath -> defectFolderPath
%
% imageFilePath - full path to the image to process
% imageFileName - image file name to process, e. g. foo.png
% defectFolderPath - path to folder where to store defect images with defects highlighted
% settingsFilePath - path to settings file to use
% csvResultFile - result file to write
% % returns 'true' - no defects, 'false' - defects found

% result_struct.status : 0 - all OK, !0 error code
% result_struct.bDefectDetected; bool true if defect detected, false - defect not detected
function result_struct_out = KCrackMainFunction( imageFilePath, imageFileName, defectFolderPath, csvResultFile, config_struct, result_struct)
fname = 'KCrackMainFunction';
logMsg('KCrackMainFunction: enter');
logMsg('KCrackMainFunction: imageFilePath = %s', imageFilePath);
logMsg('KCrackMainFunction: imageFileName = %s', imageFileName);
logMsg('KCrackMainFunction: defectFolderPath = %s', defectFolderPath);
logMsg('KCrackMainFunction: crack_sensitivity = %d, burr_sensitivity = %d', config_struct.parameters.crack_sensitivity, config_struct.parameters.burr_sensitivity);
logMsg('KCrackMainFunction: csvResultFile = %s', csvResultFile);

result_struct.status = -1;
result_struct.bDefectDetected = false;
result_struct_out = result_struct;

Ext = '.png';
filecnt = 0;

cDefects = zeros(100, 1, 'int16');
cDefects(:) = -1;           % default values
rDefects = zeros(100, 1, 'int16');
eIndices = zeros(10000, 2, 'int16');
endDefects = zeros(1,200, 'int16');

% tempDefects1=0; tempDefects2=0; tempDefects3=0; tempDefects4=0; tempDefects5=0; tempDefects6=0;
% tempDefects7=0; tempDefects8=0; tempDefects9=0; tempDefects10=0; tempDefects11=0; tempDefects12=0;

%FileName = zeros(1,256);%max path characters = 256 %commented for findImagesFiles
ImgFile = zeros(1,2);
% BW = zeros(500,1296);% define BW
eIndices = 0;
bTwoLayers = false;

% disp('...........');
% disp(imageFilePath);


if (true)
    %% read file
    logMsg('KCrackMainFunction: reading image %s', imageFilePath);
    I = imread(imageFilePath);
    
    if (ProgramConstants.DEBUG_SHOW_IMAGES)
        %'figure is necessary for the image to appear on the new figure
        figure, imshow(I);
    end
    
    %figure, imshow(I);
    %         img = Set_Defaults(img);
    [r, c, ch] = size(I);
    if ch == 3
        %find if this is rgb or gray image
        % if rgb convert to grayscale
        newI = rgb2gray(I);
        %I = 0;
        I = newI;
        logMsg('KCrackMainFunction: converted rgb image to grayscale');
    end %end of if ch==3
    
    
    
    
    % kman 2018-11-06 imageInfo does not read any global data other than
    % default parameters
    img = imageInfo(I, imageFileName, imageFilePath);
    if (ProgramConstants.DEBUG_SHOW_IMAGES)
        figure, imshow(img.Cam1Img.currImg);
        figure, imshow(img.Cam2Img.currImg);
    end
    
    
    %% read and apply from the csv text file input
    % img = readTextFileInput(img, settingsFilePath);
    % img = applyTextFileInput(img);
    
    img.below_thresh = config_struct.parameters.crack_sensitivity;
    img.burr_avg_threshold = config_struct.parameters.burr_sensitivity;
    
    close all;  % close all windows
    %         figure,imshow(A1), title(FileName);
    
    
    
    % kman 2018-11-18: imageAnalysis performs some processing on the sub-image (1 or 2)
    % and assigns imgType a value of 0, 1, or 2
    % 0 means that this images is not suitable for defect detection process
    % 1 or 2 mean that the image is suitable for some defect detection
    % (details are murky)
    logMsg('KCrackMainFunction: calling imageAnalysis: 1: imgNumber = %d', img.Cam1Img.imgNumber);
    img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting
    logMsg('KCrackMainFunction: imageAnalysis: 1: imgNumber = %d, imgType = %d, imgTypeDetails = %s', img.Cam1Img.imgNumber, img.Cam1Img.imgType, img.Cam1Img.imgTypeDetails);
    
    % kman save imgType and imgTypeDetails to preserve these values for end of
    % processing reporting
    % otherwise, they get overwritten by the consecuteive call to
    % imageAnalysis (see below)
    img.Cam1Img.imgType1 = img.Cam1Img.imgType;
    img.Cam1Img.imgTypeDetails1 = img.Cam1Img.imgTypeDetails;
    
    
    if((img.Cam1Img.imgType == 1) || (img.Cam1Img.imgType == 2))
        
        % kman 2018-11-18 - it is not clear what exactly AdaptMethod does
        % for now let's assume that that it modifies the sub-image object
        % in some way
        logMsg('KCrackMainFunction: calling AdaptMethod: imgNumber = %d', img.Cam1Img.imgNumber);
        img = AdaptMethod(img, img.Cam1Img.imgNumber);   % extract strip and find uneven edge
        logMsg('KCrackMainFunction: AdaptMethod: imgNumber = %d: returned', img.Cam1Img.imgNumber);
        
        
        % kman 2018-11-18 - now we prform imageAnalysis again. I guess it
        % make sense becasue AdaptMethod modified the image???
        logMsg('KCrackMainFunction: calling imageAnalysis: 2: imgNumber = %d', img.Cam1Img.imgNumber);
        img = imageAnalysis(img, img.Cam1Img.imgNumber);%changed function returns for plotting
        logMsg('KCrackMainFunction: imageAnalysis: 1: imgNumber = %d, imgType = %d', img.Cam1Img.imgNumber, img.Cam1Img.imgType);
        
        % kman save imgType and imgTypeDetails to preserve these values for end of
        % processing reporting
        img.Cam1Img.imgType2 = img.Cam1Img.imgType;
        img.Cam1Img.imgTypeDetails2 = img.Cam1Img.imgTypeDetails;
        
        % find uneven edge
        %
        logMsg('KCrackMainFunction: calling UnevenEdge: imgNumber = %d', img.Cam1Img.imgNumber);
        img = UnevenEdge(img, img.Cam1Img.imgNumber);
        logMsg('KCrackMainFunction: UnevenEdge: imgNumber = %d: returned', img.Cam1Img.imgNumber);
        
        logMsg('KCrackMainFunction: calling findDefects: imgNumber = %d', img.Cam1Img.imgNumber);
        [img, img.Cam1Img.unevenDef] = findDefects(img, img.Cam1Img.imgNumber);
        logMsg('KCrackMainFunction: findDefects: imgNumber = %d: returned', img.Cam1Img.imgNumber);
        
        
        
        % binary area analysis
        % find below average defects
        % kman 2018-11-18: binAreaAnalysis does something super simple
        % not sure why it is a separate function call and not a part of findBelowAvgDef
        newBArea = binAreaAnalysis(img.Cam1Img.IBin);
        
        logMsg('KCrackMainFunction: calling findBelowAvgDef: imgNumber = %d', img.Cam1Img.imgNumber);
        img.Cam1Img.belowAvgDef = findBelowAvgDef(img, img.Cam1Img.imgNumber, newBArea);
        % displayDefects(img, img.Cam1Img.imgNumber, img.Cam1Img.belowAvgDef);
        logMsg('KCrackMainFunction: findBelowAvgDef: imgNumber = %d: returned', img.Cam1Img.imgNumber);
        
        logMsg('KCrackMainFunction: calling findBurr: imgNumber = %d', img.Cam1Img.imgNumber);
        img = findBurr(img, img.Cam1Img.imgNumber);
        logMsg('KCrackMainFunction: findBurr: imgNumber = %d: returned', img.Cam1Img.imgNumber);
        
    end
    
    % kman 2018-11-18 - there is a potential bug here as the second call to
    % imageAnalysis above modifies imgType
    
    if((img.Cam1Img.imgType == 1) || (img.Cam1Img.imgType == 2))
        img.Cam1Img.unevenDefCount = nnz(img.Cam1Img.unevenDef);
        img.Cam1Img.belowAvgDefCount = nnz(img.Cam1Img.belowAvgDef);
        img.Cam1Img.burrDefCount = nnz(img.Cam1Img.burrDef);
        
        img.Cam1Img.endDefects = [img.Cam1Img.unevenDef img.Cam1Img.belowAvgDef img.Cam1Img.burrDef ];
        img.Cam1Img.endDefects( :, ~any(img.Cam1Img.endDefects,1) ) = [];  %remove all columns with zeros
        
        img.Cam1Img.endDefCount = nnz(img.Cam1Img.endDefects);
    else
        img.Cam1Img.unevenDefCount = 0;
        img.Cam1Img.belowAvgDefCount = 0;
        img.Cam1Img.burrDefCount = 0;
        img.Cam1Img.endDefCount = 0;
    end
    
    
    close all;  % close all windows
    
    
    % kman 2018-11-18: below comes copy and paste of the code above but the
    % processing applied to Camera 2 image
    % I am not familiar enough with matlab objects/handles/references rules
    % to merge it so I am letting this go in the first pass
    
    
    logMsg('KCrackMainFunction: calling imageAnalysis: 1: imgNumber = %d', img.Cam2Img.imgNumber);
    img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
    logMsg('KCrackMainFunction: imageAnalysis: 1: imgNumber = %d, imgType = %d, imgTypeDetails = %s', img.Cam2Img.imgNumber, img.Cam2Img.imgType, img.Cam2Img.imgTypeDetails);
    
    % kman save imgType and imgTypeDetails to preserve these values for end of
    % processing reporting
    % otherwise, they get overwritten by the consecuteive call to
    % imageAnalysis (see below)
    img.Cam2Img.imgType1 = img.Cam2Img.imgType;
    img.Cam2Img.imgTypeDetails1 = img.Cam2Img.imgTypeDetails;
    
    if((img.Cam2Img.imgType == 1) || (img.Cam2Img.imgType == 2))
        
        logMsg('KCrackMainFunction: calling AdaptMethod: imgNumber = %d', img.Cam2Img.imgNumber);
        img = AdaptMethod(img, img.Cam2Img.imgNumber);     % extract strip and find uneven edge
        logMsg('KCrackMainFunction: AdaptMethod: imgNumber = %d: returned', img.Cam2Img.imgNumber);
        
        logMsg('KCrackMainFunction: calling imageAnalysis: 2: imgNumber = %d', img.Cam2Img.imgNumber);
        img = imageAnalysis(img, img.Cam2Img.imgNumber);%changed function returns for plotting
        logMsg('KCrackMainFunction: imageAnalysis: 1: imgNumber = %d, imgType = %d', img.Cam2Img.imgNumber, img.Cam2Img.imgType);
        
        % kman save imgType and imgTypeDetails to preserve these values for end of
        % processing reporting
        img.Cam2Img.imgType2 = img.Cam2Img.imgType;
        img.Cam2Img.imgTypeDetails2 = img.Cam2Img.imgTypeDetails;
        
        %find uneven edge
        %
        logMsg('KCrackMainFunction: calling UnevenEdge: imgNumber = %d', img.Cam2Img.imgNumber);
        img = UnevenEdge(img, img.Cam2Img.imgNumber);
        logMsg('KCrackMainFunction: UnevenEdge: imgNumber = %d: returned', img.Cam2Img.imgNumber);
        
        logMsg('KCrackMainFunction: calling findDefects: imgNumber = %d', img.Cam2Img.imgNumber);
        [img, img.Cam2Img.unevenDef] = findDefects(img, img.Cam2Img.imgNumber);
        logMsg('KCrackMainFunction: findDefects: imgNumber = %d: returned', img.Cam2Img.imgNumber);
        
        
        %binary area analysis
        newBArea = binAreaAnalysis(img.Cam2Img.IBin);
        
        logMsg('KCrackMainFunction: calling findBelowAvgDef: imgNumber = %d', img.Cam2Img.imgNumber);
        img.Cam2Img.belowAvgDef = findBelowAvgDef(img, img.Cam2Img.imgNumber, newBArea);
        logMsg('KCrackMainFunction: findBelowAvgDef: imgNumber = %d: returned', img.Cam2Img.imgNumber);
        
        logMsg('KCrackMainFunction: calling findBurr: imgNumber = %d', img.Cam2Img.imgNumber);
        img = findBurr(img, img.Cam2Img.imgNumber);
        logMsg('KCrackMainFunction: findBurr: imgNumber = %d: returned', img.Cam2Img.imgNumber);
        
    end
    
    if((img.Cam2Img.imgType == 1) || (img.Cam2Img.imgType == 2))
        img.Cam2Img.unevenDefCount = nnz(img.Cam2Img.unevenDef);
        img.Cam2Img.belowAvgDefCount = nnz(img.Cam2Img.belowAvgDef);
        img.Cam2Img.burrDefCount = nnz(img.Cam2Img.burrDef);
        
        img.Cam2Img.endDefects = [img.Cam2Img.unevenDef img.Cam2Img.belowAvgDef img.Cam2Img.burrDef];
        img.Cam2Img.endDefects( :, ~any(img.Cam2Img.endDefects,1) ) = [];  %remove all columns with zeros
        
        img.Cam2Img.endDefCount = nnz(img.Cam2Img.endDefects);
    else
        img.Cam2Img.unevenDefCount = 0;
        img.Cam2Img.belowAvgDefCount = 0;
        img.Cam2Img.burrDefCount = 0;
        img.Cam2Img.endDefCount = 0;
    end
    
    close all;
    
    % kman 2019-03-28: because I have seen displayFinalDefects throw an
    % exceptoin, dor try/catch here
    try
        logMsg('%s: calling displayFinalDefects: %s', fname, defectFolderPath);
        result_struct_out = displayFinalDefects(img, defectFolderPath, result_struct_out);
        logMsg('%s: displayFinalDefects returned: status = %d, bDefectDetected = %d', fname, result_struct_out.status, result_struct_out.bDefectDetected);
    catch ME
        logMsg('%s: exception in displayFinalDefects: %s %s', fname, ME.identifier, ME.message);
        result_struct_out.status = -1;
        %continue even if displayFinalDefects threw an exception so that
        %this recorded in csvResultFile
    end
    
    
    
    % save results
    lfid = -1;
    if (not(isempty(csvResultFile)))
        % this is how internet teaches us to check for file existance
        if (exist(csvResultFile, 'file') == 2)
            % file exists
        else
            % file does not exist - write header
            lfid = fopen(csvResultFile, 'a');
            if(lfid > 0)
                fprintf(lfid, 'imageFilePath,imageFileName,defectFolderPath,crack_sensitivity,burr_sensitivity,defect');
                fprintf(lfid, ',1:Type1,1:Details1,1:Type2,1:Details2,1:end,1:uneven,1:below,1:burr');
                fprintf(lfid, ',2:Type1,2:Details1,2:Type2,2:Details2,2:end,2:uneven,2:below,2:burr');
                fprintf(lfid, '\n');
                fclose(lfid);
                lfid = -1;
            end
        end
        
        %open again to write results
        lfid = fopen(csvResultFile, 'a');
        if(lfid > 0)
            fprintf(lfid, '%s,%s,%s,%d,%d', imageFilePath, imageFileName, defectFolderPath, config_struct.parameters.crack_sensitivity, config_struct.parameters.burr_sensitivity);
            if (result_struct_out.status == 0)
                if (result_struct_out.bDefectDetected)
                    fprintf(lfid, ',1');
                else
                    fprintf(lfid, ',0');
                end
            else
                fprintf(lfid, ',-1'); %to indicate error
            end
            
            
            fprintf(lfid, ',%d,"%s",%d,"%s"', img.Cam1Img.imgType1, img.Cam1Img.imgTypeDetails1, img.Cam1Img.imgType2, img.Cam1Img.imgTypeDetails2);
            fprintf(lfid, ',%d,%d,%d,%d', img.Cam1Img.endDefCount, img.Cam1Img.unevenDefCount, img.Cam1Img.belowAvgDefCount, img.Cam1Img.burrDefCount);
            fprintf(lfid, ',%d,"%s",%d,"%s"', img.Cam2Img.imgType1, img.Cam2Img.imgTypeDetails1, img.Cam2Img.imgType2, img.Cam2Img.imgTypeDetails2);
            fprintf(lfid, ',%d,%d,%d,%d', img.Cam2Img.endDefCount, img.Cam2Img.unevenDefCount, img.Cam2Img.belowAvgDefCount, img.Cam2Img.burrDefCount);
            fprintf(lfid, '\n');
            fclose(lfid);
            lfid = -1;
        end
        
    else
        logMsg('KCrackMainFunction: empty csvResultFile = %s', csvResultFile);
    end
    
    
end % if (true)


end % end of function

