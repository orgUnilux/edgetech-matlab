function status = logStatus(format, varargin)
% logStatus Summary of this function goes here
%   Detailed explanation goes here

global logStatusFileName;
fname = 'logStatus';

try
    
    token = sprintf(format, varargin{:});
    
    
    lfid = fopen(logStatusFileName, 'a+');
    %lfid = fopen('D:\\ImageTestSets\\01-QuickTest\\Images\\Defects\\test%d.log', 'a+');
    
    if(lfid > 0)
        fprintf(lfid, '%s', token);
        fclose(lfid);
    else
        status = 1;
        return;
    end
catch ME
    logMsg('%s: exception: %s %s', fname, ME.identifier, ME.message);
    status = 2;
    return;
end

status = 0;
end

