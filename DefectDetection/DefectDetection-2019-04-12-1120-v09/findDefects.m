%% This function finds the derivative of the input values. The input value 
% could be just one side boundary of the binary images or it could be a
% summation of the binary image.
% Here the derivative values that deviate are taken into consideration as
% mid value of the defect. Other than that 0% region detected is also
% considered as defect. 
% Once the mid values of defects are determined, the end indices are 
% defined as (defect index - 15) to (defects index + 15)
%function [cDefects] = findDefects(newBMin)
%function [endDefects] = findDefects(newBMin, endDefects)
function [img, endDefects] = findDefects(img, imgNum)
logMsg('findDefects: enter: imgNum = %d', imgNum);

if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end
endDefects = zeros(1,200, 'int16');
% endDefects(:) = 0;
tempDefects = size(endDefects);
newDiff = diff(imgTemp.newBMin); % find derivative % newDiff = diff(newBMax); % find derivative
% new2Diff = diff(newDiff); % 2nd deriv

[rDefects,cDefects] = find ( (newDiff >= 2) | (newDiff <= -2)); %[rDefects,cDefects] = find ( (newDiff >= 7) | (newDiff <= -7));

%% find 0% cut region and save the indices
%find the end indices, assume that one instance of 0% region exists
% if not this method detects all these regions as one single region
cDefZeros = find(imgTemp.newBMin == 0);
cDefZerosIndices = zeros(1,2);
[~,cDefZerosSize] = size(cDefZeros);
if(cDefZerosSize > 1)
    cDefZerosIndices(1,1) = cDefZeros(1,1);
    cDefZerosIndices(1,2) = cDefZeros(1,cDefZerosSize);
end
% 
% % below average values. find the start and end indices, this defined as
% % defect later as the value is below average
% newBAvg = mean(newBMin);
% cDefBelAvg = find(newBMin <= newBAvg);
% cDefBelAvgIndices = zeros(1,2);
% [~,cDefBelAvgSize] = size(cDefBelAvg);
% if(cDefBelAvgSize > 1)
%     cDefBelAvgIndices(1,1) = cDefBelAvg(1,1);
%     cDefBelAvgIndices(1,2) = cDefBelAvg(1,cDefBelAvgSize);
% end

%%find dupicates in cDefects
[~,cCnt] = size(cDefects);

resetDef = zeros(size(cDefects)); % save the defects indices which should be reset(not to consider the inbetween values)
cnt = 0; % initialize cnt
cntCscv = 0; % initialize count for the consecutive indices detected
currDef = 0;
nextDef = 0;
prevDef = 0;
if cCnt > 1
    
    imgTemp.bSaveDefImg = true;
    for indx = 2:(cCnt-1)
        currDef = cDefects(1, indx);
        prevDef = cDefects(1, indx - 1);
        nextDef = cDefects(1, indx + 1);
        if( currDef < 20 ) || ( currDef > 1276 )
            % avoid picking the values on the extreme end of images
            resetDef(1, indx) = -1;
            if(indx == 2)
                resetDef(1, 1) = -1;
            end
        end
        %        if (cDefects(1,indx) >= (cDefects(1,(indx-1)) + 40))
        %            %
        %            cDefects(1,(indx-1)) = -1;
        %if (cDefects(1,indx+1) <= (cDefects(1,indx) + 100))
        % compare 3 consecutive indices. If condition is met reset the
        % middle index
        if  (nextDef <= (currDef + 5)) && ( currDef <= (prevDef + 5)) 
            % find if the indices are very close(consecutive or 5 pixels 
            % apart) to each other and within 20. This is not considered 
            % as defect 
            cntCscv = cntCscv + 1;
            resetDef(1, indx) = -1;
        end
    end %end for
else
    logMsg('findDefects:87: leave: imgNum = %d: no defects', imgNum);
    return; %no defects
end %end if

%% finally if any censecutive closeby indices found reset them
for indx = 1:cCnt
    if resetDef(1, indx) == -1
        %fprintf(img.fileID, '%d    ', indx);
        % kman 2018-11-07 disp(indx);
        cDefects(1, indx) = 0; 
    end
end

if(cCnt > 0)
% sort the indices
SortedDef = sort(cDefects);
%remove reset values(-1)
% cDefects(:) = 0;
[~,~,cDefects] = find(SortedDef);
end

tempDefects = defectPointToRange(tempDefects, cDefects);

if(cDefZerosSize > 1)
    %we have defect at cDefZerosIndices
    %concatenate them 2016-05-12
    tempDefects = [tempDefects cDefZerosIndices];
end

[~, eEle] = find(tempDefects);

if isempty(eEle) 
    %no defects
    tempDefects = NaN;
    endDefects = tempDefects;
else
    [~,esize] = size(eEle);
    
    if(isnan(tempDefects))
        if(esize ~= 1)
            endDefects = NaN;
        end
    else
        %     [~,esize] = size(eEle);
        % resize the defect array size to non-zero elements
        %             endDefects = reshape(tempDefects, [],esize);
        
        %remove all columns with zeros
        tempDefects( :, ~any(tempDefects,1) ) = [];
        endDefects = tempDefects;
    end
end

% finally at the end of this function save the changes made to indivudal 
% image object
if(imgNum == 1)
    img.Cam1Img = imgTemp;
else
    img.Cam2Img = imgTemp;
end

logMsg('findDefects: leave: imgNum = %d', imgNum);
end % end of fuction