%% this function pens file nameed'DD_yyyy-mm-dd.log'
% logs thje  'message' in the file
% closes the file after logging
function message = LogMessage( message)

%%open log file
lfid = -1; %% log file default id
% choose the folder for defect setings file
% Program Files (x86) or Program Files
ConfigPath_64b = 'C:\Program Files (x86)\Unilux\EdgeTech\config\';
ConfigPath_32b = 'C:\Program Files\Unilux\EdgeTech\config\';
settingFilename = 'DefectConfig.txt';

if(isdir(ConfigPath_64b))
    fullpath = strcat(ConfigPath_64b, settingFilename);
else
    fullpath = strcat(ConfigPath_32b, settingFilename);
end

settingFilename = 'DefectConfig.txt';
formatOut = 'yyyy-mm-dd';
fileSuffix = datestr(now,formatOut);
logfilename = strcat('DD_',fileSuffix, '.log');
logTempPath = strrep(fullpath, 'config', 'logs\Defect_logs' );
logFullPath = strrep(logTempPath, settingFilename, logfilename);
%logFullPath = strcat(logFullPath, 'DD_',date, '.log');
%logfilename = strcat('DD_',date, '.log');
lfid = fopen(logFullPath, 'a+');
if(lfid > 0)
 temp = message;
 
%%get date time string array
 t = datestr(datetime('now','TimeZone','local','Format','MM/yyyy//dd HH:mm:ss'));
 %%write logg message to file
log = sprintf("%s EdgeTech::%s\n", t, message);
fwrite(lfid, log);
% now that we wrote successfully to the log file delete the string
erase(message,temp); 
rstat = fclose(lfid);
end % end  of if
end %end of function