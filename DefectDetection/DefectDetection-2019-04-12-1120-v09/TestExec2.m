function status = TestExec2(varargin)

fprintf('Total number of inputs (nargin) = %d\n', nargin);

nVarargs = length(varargin);
fprintf('Inputs in varargin(%d):\n',nVarargs)
for k = 1:nVarargs
      fprintf('%d: %s\n', k, varargin{k})
end



status = 0; 

end
