% this function takes binary area as input arguement finds the value that
% is below average . this method is used to determine the

%function cDefects = findBelowAvgDef(newBMin)
function cDefects = findBelowAvgDef(img, imgNum, newBMin)
% tempDefIndxStart = 1; % set start point as first defect
% tempDefIndxEnd = 2;       % set end point as first defect

if(imgNum == 1)
    imgTemp = img.Cam1Img;
else
    imgTemp = img.Cam2Img;
end

cDefects = NaN;
% cDefects = zeros([1 200]);
% below average values. find the start and end indices, this defined as
% defect later as the value is below average
newBAvg = mean(newBMin);
bfoundStart = false;
% introduce threshold for the  average value
newBAvg = newBAvg - img.below_thresh;

% plot(newBMin);
% hold on;
% aveall(1, 1:imgTemp.iWidth) = newBAvg;
% plot(aveall);
% hold off;

cDefBelAvg = find(newBMin <= newBAvg);
% cDefBelAvgIndices = zeros(1,2);
[~,cDefBelAvgSize] = size(cDefBelAvg);
if(cDefBelAvgSize > 1) %commented on 2016-05-12      
    cnt = 1;    
    % % below algo commented on 2016-05-12
    for indx = 2 : imgTemp.iWidth        
        if(cnt >= DefectConstants.MAX_DEFECTS_CNT)
            break; % we have enough defects
        end
        if( (newBMin(1,indx) >= newBAvg) && (newBMin(1,indx - 1) <= newBAvg) && (bfoundStart == true))
            cDefects(1,cnt+1) = indx;
            cnt = cnt+2; % increment to find next defect
            bfoundStart = false;
            
            imgTemp.bSaveDefImg = true;            
        end
        if( (newBMin(1,indx) <= newBAvg) && (newBMin(1,indx - 1) >= newBAvg) )
            cDefects(1, cnt) = indx;
            bfoundStart = true;
            
            imgTemp.bSaveDefImg = true;            
        end %end of if
    end %end of for
    if(cDefects(1,1) == 0)
        cDefects(1,1) = 1; %set this value
    end
end

[~, eEle] = find(cDefects);

if(~isnan(cDefects))
    if isempty(eEle)
        %no defects
        cDefects = NaN;
    else
        if mod(eEle, 2)
            %we found start point but not end point
            % bouindary condition
            %end point is end of the image
            cDefects(eEle + 1) = imgTemp.iWidth - 1; %default last value to
        end
    end
end
end %end of function
